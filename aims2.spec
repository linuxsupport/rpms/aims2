Name: aims2
Version: 2.74
Release: 1%{?dist}
Summary: Automated Installation Management Server (v.2)
Group: System Environment/Daemons
Source0: %{name}-%{version}.tgz
License: GPL+
Vendor: CERN
Packager: Linux.Support@cern.ch
URL: https://cern.ch/linux/
BuildArch: noarch

%description
Automated Installation Management Server (v.2)
Documentation at: https://linux.web.cern.ch/installation/aims/aims2client/

%if 0%{?rhel} == 8
# Only build aims2-server for el8. We only support 7 for the client package.
%package -n aims2-server
Summary: AIMS2 server of Automated Installation Management Server (v.2)
Group: System Environment/Daemons

Requires: perl >= 5.8.5
Requires: perl-SOAP-Lite >= 0.65
Requires: perl-Authen-Krb5 >= 1.5
Requires: perl-Net-DNS >= 0.48
Requires: perl-DBI >= 1.40
Requires: perl-DBD-Pg >= 2.19.3
Requires: perl-LDAP >= 0.31
Requires: tftp-server >= 0.39
Requires: perl-DateManip >= 5.42
Requires: perl-SQL-Abstract >= 1.22
# This is not a requirement but helps adding more logs to debug
Requires: perl-DBI-Log
Requires: httpd >= 2.0.52
Requires: perl-IO-Socket-INET6
Requires: perl-Socket6
Requires: perl-CGI
Requires: perl-String-Random >= 0.25
Requires: perl-File-Find-Rule >= 0.33

Requires: systemd
Requires: logrotate
BuildRequires:    systemd
Requires(post):   systemd
Requires(preun):  systemd
Requires(postun): systemd

Provides: aims2server = %{version}
Obsoletes:aims2server < %{version}


#until we rename modules correctly ...
Provides: perl(aims2server::conf), perl(aims2server::helpers), perl(aims2server::db), perl(aims2server::host), perl(aims2server::image), perl(aims2server::landb), perl(aims2server::ldap), perl(aims2server::pxe), perl(aims2server::server), perl(aims2server::soap)

%description -n aims2-server
Server-side software for AIMS2 remote installations service.
%endif

%package -n aims2-client
Summary: AIMS2 client of Automated Installation Management Server (v.2)
Group: System Environment/Base

Requires:  perl >= 5.8.5
Requires:  perl-SOAP-Lite >= 0.65
Requires:  perl-Authen-Krb5 >= 1.5
Requires:  perl-Net-DNS >= 0.48
Provides:  aims2client = %{version}
Obsoletes: aims2client < %{version}
Requires:  perl-Socket6
Requires:  perl-IO-Socket-INET6

%description -n aims2-client
Client-side software for using the AIMS2 remote installations service.
See: https://linuxops.web.cern.ch/aims2/aims2/
for more information.

%prep

%setup -q

%build

%install
pod2man aims2client > aims2client.1
mkdir -p %{buildroot}/usr/{bin,sbin}/
mkdir -p %{buildroot}/usr/share/man/man1/
# man entries need to be prepared so rpmbuild does not complain about aims2 man entry missing
ln -sf aims2.1 %{buildroot}/usr/share/man/man1/aims2client.1
install -p -m 644 aims2client.1 %{buildroot}/usr/share/man/man1/
%if 0%{?rhel} == 8
MODULES="conf.pm db.pm host.pm image.pm landb.pm ldap.pm pxe.pm server.pm soap.pm helpers.pm"
CGIS="ks.cgi reboot.cgi server.cgi upload.cgi"
mkdir -p %{buildroot}/var/log/aims/
mkdir -p %{buildroot}/usr/share/perl5/aims2server/
mkdir -p %{buildroot}/var/www/cgi-bin/aims2server/
mkdir -p %{buildroot}/etc/httpd/conf.d/
mkdir -p %{buildroot}/usr/lib/systemd/system/
mkdir -p %{buildroot}/etc/logrotate.d/
mkdir -p %{buildroot}/var/lock/subsys/aims
install -p -m 755 aims2config %{buildroot}/usr/sbin/
install -p -m 755 aims2cleanup %{buildroot}/usr/sbin/
install -p -m 755 aims2oldhostsremoval %{buildroot}/usr/sbin/
for CGI in $CGIS; do \
    install -p -m 755 cgi/$CGI %{buildroot}/var/www/cgi-bin/aims2server/; \
done
for MODULE in $MODULES; do \
    install -p -m 644 aims2server/$MODULE %{buildroot}/usr/share/perl5/aims2server/; \
done
install -p -m 644 cfg/aims2httpd.conf %{buildroot}/etc/httpd/conf.d/
install -p -m 644 cfg/aims2cleanup.service %{buildroot}/usr/lib/systemd/system/
install -p -m 644 cfg/aims2.conf %{buildroot}/etc/
install -p -m 644 cfg/aims2cleanup.logrotate %{buildroot}/etc/logrotate.d/aims2cleanup
%endif
ln -sf aims2client %{buildroot}/usr/bin/aims2
install -p -m 755 aims2client %{buildroot}/usr/bin/
cd %{buildroot}/usr/share/man/man1
ln -s aims2client.1.gz aims2.1.gz
cd -

%if 0%{?rhel} == 8
%post
%systemd_post aims2cleanup.service

%preun
%systemd_preun aims2cleanup.service

%postun
%systemd_postun_with_restart aims2cleanup.service
%endif

# Only install aims2-server components for el8
%if 0%{?rhel} == 8
%files -n aims2-server
%defattr(-, root, root)
/usr/sbin/aims2cleanup
/usr/sbin/aims2config
/usr/sbin/aims2oldhostsremoval
#/etc/init.d/aims2cleanup
%{_unitdir}/aims2cleanup.service
/etc/logrotate.d/aims2cleanup
%config(noreplace) /etc/aims2.conf
%config(noreplace) /etc/httpd/conf.d/aims2httpd.conf
/usr/share/perl5/aims2server/*.pm
#/usr/lib/perl5/site_perl/aims2server/*.pm
/var/www/cgi-bin/aims2server/*.cgi
%dir /var/log/aims/
%endif

%files -n aims2-client
%defattr(-, root, root)
/usr/bin/aims2
/usr/bin/aims2client
/usr/share/man/man1/*

%changelog

* Thu Apr 04 2024 Ben Morrice <ben.morrice@cern.ch> - 2.74-1
- Fix: aims2client should be smart enough for fqdn provided user input

* Wed Mar 27 2024 Ben Morrice <ben.morrice@cern.ch> - 2.73-1
- Fix regression: permit dyndns.cern.ch hosts (INC3795354)

* Fri Mar 15 2024 Alex Iribarren <Alex.Iribarren@cern.ch> - 2.72-1
- Add support for PDC

* Mon Jul 11 2022 Daniel Juarez <djuarezg@cern.ch> - 2.71-1
- Rename uefi as arm64-efi or x86_64-efi, boot mode and arch are independent

* Fri Jul 01 2022 Daniel Juarez <djuarezg@cern.ch> - 2.70-2
- Increase aims2cleanup delay and make it random

* Mon May 02 2022 Daniel Juarez <djuarezg@cern.ch> - 2.70-1
- Fix aims2cleanup

* Fri Apr 29 2022 Daniel Juarez <djuarezg@cern.ch> - 2.69-1
- Ref: OTG0069753. Remove fake Legacy boot flag from client side

* Mon Feb 28 2022 Daniel Juarez <djuarezg@cern.ch> - 2.65-1
- Deprecate BIOS legacy workaround

* Wed Oct 06 2021 Daniel Juarez <djuarezg@cern.ch> - 2.64-3
- ARM64 grub confs now use HTTP as download method to avoid timeouts

* Tue Oct 05 2021 Daniel Juarez <djuarezg@cern.ch> - 2.63-1
- Switch from PSQL date format to timestamp for finer granularity
- Remove unused ORA type references

* Mon Sep 06 2021 Daniel Juarez <djuarezg@cern.ch> - 2.62-1
- Migrate from Oracle DB to PSQL
- Deprecate ks kernel param in favour of inst.ks

* Mon Jul 12 2021 Daniel Juarez <djuarezg@cern.ch> - 2.61-1
- Remove all PXE image blobs references for updated DB schema

* Mon Jul 12 2021 Daniel Juarez <djuarezg@cern.ch> - 2.60-1
- Do not store image blobs in DB

* Wed Jun 30 2021 Daniel Juarez <djuarezg@cern.ch> - 2.59-1
- EL7 backwards compatibility

* Tue Jun 29 2021 Daniel Juarez <djuarezg@cern.ch> - 2.58-1
- LOS-615: Temporary workaround, removing (http) method as some nodes timeout due to slow download speeds

* Thu Jun 03 2021 Daniel Juarez <djuarezg@cern.ch> - 2.57-1
- Deprecate clients older than 2.33

* Tue May 18 2021 Daniel Juarez <djuarezg@cern.ch> - 2.56-1
- Fix 24h expiration to set to localboot

* Thu Apr 29 2021 Daniel Juarez <djuarezg@cern.ch> - 2.55-1
- Use DBI::Log to gather stats about all DB transactions

* Fri Apr 23 2021 Daniel Juarez <djuarezg@cern.ch> - 2.54-1
- Adding nightly host cleanup. 24h to localboot, 60 days max host retention

* Mon Apr 19 2021 Daniel Juarez <djuarezg@cern.ch> - 2.52-1
- Allow --force on image uploads for cases where image already existed

* Fri Apr 16 2021 Daniel Juarez <djuarezg@cern.ch> - 2.51-1
- Minor fixes and more logging

* Mon Apr 12 2021 Daniel Juarez <djuarezg@cern.ch> - 2.50-1
- Update DB schema for the AIMS redesign

* Tue Apr 06 2021 Daniel Juarez <djuarezg@cern.ch> - 2.49-1
- Do not remove pxe confs on aims2cleanup to avoid race condition

* Wed Mar 31 2021 Daniel Juarez <djuarezg@cern.ch> - 2.48-1
- Removing LANDB checks for removing hosts for Ironic workflows

* Tue Mar 30 2021 Daniel Juarez <djuarezg@cern.ch> - 2.47-1
- Improve aims2cleanup dependencies for CephFS share automounting

* Mon Dec 14 2020 Daniel Juarez <djuarezg@cern.ch> - 2.46-1
- Remove and clean dead code
- Clarify lots of code pieces
- Fix aims2config so we can manage DB conf
- Improve logging to ease troubleshooting
- Make it scalable, multiple hosts sharing content with Ceph
- It simulates the old sync behaviour for temp intermediate migration

* Wed Dec 02 2020 Daniel Juarez <djuarezg@cern.ch> - 2.45-1
- Add perl-CGI dependency por C8

* Wed Nov 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.44-1
- Build server components for C8 as well

* Wed Nov 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.43-1
- Use linuxefi module instead of linux, LOS-627

* Fri Oct 16 2020 Daniel Juarez <djuarezg@cern.ch> - 2.42-1
- Disable autocommit on pxe conf to reduce DB transaction times

* Fri Sep 25 2020 Daniel Juarez <djuarezg@cern.ch> - 2.41-1
- Standarise grub linux and initrd directives after grub2 update

* Fri Sep 25 2020 Daniel Juarez <djuarezg@cern.ch> - 2.40-1
- Only update DB iface status when removing pxeconfs

* Fri Sep 18 2020 Daniel Juarez <djuarezg@cern.ch> - 2.39-1
- Reduce unnecessary long delays on aims2sync

* Tue Sep 8 2020 Daniel Juarez <djuarezg@cern.ch> - 2.38-1
- Split pxeboot and pxeconf building into two processes

* Mon Sep 7 2020 Daniel Juarez <djuarezg@cern.ch> - 2.37-1
- Grub UEFI conf building was wrongly comparing archs

* Fri Aug 7 2020 Daniel Juarez <djuarezg@cern.ch> - 2.36-1
- Update man content, and fix man symlinks

* Wed Aug 5 2020 Daniel Juarez <djuarezg@cern.ch> - 2.35-1
- C8 client was breaking ai-tools workflow

* Fri Jul 24 2020 Daniel Juarez <djuarezg@cern.ch> - 2.34-1
- Do image uploads through HTTP to avoid memory spikes

* Mon Jul 6 2020 Daniel Juarez <djuarezg@cern.ch> - 2.33-1
- C8 aims2-client

* Mon Jun 15 2020 Daniel Juarez <djuarezg@cern.ch> - 2.32-1
- Add armv7 and armv8 archs
- Bugfix: initrd img artifact is again optional
- Bugfix: checking image existence should not use arch nor uefi mode

* Thu Dec 05 2019 Daniel Juarez <djuarezg@cern.ch> - 2.31-1
- Adding PXEBoot media was ignoring egroups

* Mon Nov 18 2019 Daniel Juarez <djuarezg@cern.ch> - 2.30-1
- Use MIME attachments for image uploading to reduce memory usage

* Mon Nov 04 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29-3
- Check PXE images existence before uploading

* Fri Nov 01 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29-2
- Add building 6045 to the datacentre list for LHCb container

* Fri Nov 01 2019 Daniel Juarez <djuarezg@cern.ch> - 2.29
- Make UEFI images download through HTTP

* Wed Sep 18 2019 Daniel Abad <d.abad@cern.ch> - 2.28
- Increase initrd limit from 0.5 to 1.0 GB

* Tue May 07 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.27-2
- Remove aims2client future
- Fix global hostname override

* Fri May 03 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.27-1
- Add building 773 to the datacentre list

* Thu Apr 11 2019 Ulrich Schwickerath <ulrich.schwickerath@cern.ch> - 2.26
- improve support for IPv6 installations

* Tue Feb 19 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.25
- fix cleanup. Re-enable http:// kickstart location

* Fri Aug 17 2018 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.20
- port server to CentOS 7
- change system to use 3 sync hosts.
- change server alias
- move to systemd

* Thu Aug 03 2017 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.16
- add check for LandbManagerPerson for host owners.

* Thu Apr 21 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.15
- add uefi/SLC6/RHEL6 booting possibility

* Tue Apr 05 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13.4
- recursive e-group membership search

* Wed Mar 02 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13.1
- added uefi/aarch64 support for pxe images.

* Tue Mar 01 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.13
- added arm64 boot support
- added addhost command options for uefi/lgcy/arm64

* Fri Feb 26 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.11
- bios/uefi/lgcy boot with dnsmasq support

* Tue Feb 23 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.10-1
- adding bios/uefi/lgcy boot.

* Mon Jan 18 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.22-1
- change boot protocol to http (aims2sync)

* Thu Jan 14 2016 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.21-1
- image upload permissions for CMF admins. (server)

* Mon Jul 20 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.19-2
- fix actual perl-Socket6 and perl-IO-Socket-INET6 requires in
  aims2-client package.

* Thu Apr 30 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.19-1
- add perl-Socket6 and perl-IO-Socket-INET6 as deoendencies
  to make the client work on IPv6-enabled systems too.
- (will still use IPv4)

* Wed Jan  7 2015 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.17
- CDB checks in server disabled, quattor is off since nov 2014
* Fri Mar 15 2013 Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.16
- display single sync server status in client '--full' option
* Wed Apr  4 2012   Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.14
- added nested e-groups support for host permission checking.
* Mon Apr  4 2011  Jaroslaw Polok <jaroslaw.polok@cern.ch> - 2.9.9
- added pxe noexpiry option
* Thu Oct 21 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 2.9.6-1
- fix CDBSQL SQL statement in preparation for using dedicated ITCORE account

* Fri Aug 27 2010  KELEMEN Peter <Peter.Kelemen@cern.ch> - 2.9.5-1
- error handling fixes
- added logrotate
