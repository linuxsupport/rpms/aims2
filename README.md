# AIMS2: Automated Installation Management System (v. 2)

Take into account that Ironic, ai-tools and possibly procurement scripts make use of this tool, keep in mind this when altering the behaviour significantively without letting them know.

## CentOS 7 deployments

**IMPORTANT: YOU WILL NEED TO COPY RPMS TO `/mnt/data2/home/build/packages/incoming/cc7/cern` SO THEY BECOME AVAILABLE ON THE [`CERN`](http://linuxsoft.cern.ch/cern/centos/7/cern) REPO**

CC7 versions are no longer required. AIMS is running on RHEL8. `aims2client` will be used from `aiadm` which is RHEL8/9.

## Database schema

Ref. <https://linux.web.cern.ch/installation/aims/aims2client/>

Create DB database and `aims` user:


```
# psql -h dbod-aims.cern.ch -U admin -p 6601
CREATE USER aims WITH PASSWORD 'THEPASSWORD';
ALTER USER aims VALID UNTIL 'infinity';
GRANT aims TO admin;
CREATE DATABASE aims OWNER aims;
GRANT ALL PRIVILEGES ON DATABASE aims TO aims;
CREATE SCHEMA AUTHORIZATION aims;
\c aims;
GRANT CONNECT ON DATABASE aims TO aims;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA aims TO aims;
```

Modify the instance's [`pg_hba.conf file`](https://dbod-user-guide.web.cern.ch/instance_management/configuration_files.html)to allow the new user to connect and add this line at the end:

```
host    aims    aims    0.0.0.0/0    md5
```

Restart the DBoD instance or it will not take this config change into account.

Create the tables:

```
create table conf ( key varchar(32) not null primary key, value varchar(400) not null);

create table hardware ( hostname varchar(100) not null , hw varchar(32) not null );
alter table hardware add constraint HW_PK unique (hw);

create table kickstart ( hostname varchar(100) not null , ks text, source varchar(500) not null, lastupdated timestamp username varchar(32) );

create table owners ( owner varchar(100) not null , egroups varchar(100) not null );

create table pxeboot ( name varchar(100) not null, arch varchar(12), description varchar(500), owner varchar(100), initrd_source varchar(500), initrd_sum varchar(32), initrd_size numeric(10), vmlinuz_source varchar(500), vmlinuz_sum varchar(32), vmlinuz_size numeric(10), kopts varchar(500), uploaded timestamp, groups varchar(100), uefi numeric(1));
alter table pxeboot add constraint BOOT_PK unique (name);

create table pxehosts ( hostname varchar(100) not null, status numeric(1) not null, target varchar(100) not null, username varchar(100) not null, kopts varchar(500), registered timestamp enabled timestamp booted timestamp disabled timestamp noexpiry numeric(38), type numeric(1) );
alter table pxehosts add constraint HOST_PK unique (hostname);

DB schema:

SQL> select table_name from user_tables;

table_name
------------------------------
conf
hardware
kickstart
owners
pxeboot
pxehosts

sql> desc conf
 NAME                      NULL?    TYPE
 ----------------------------------------- -------- ----------------------------
 key                       not null varchar(32)
 value                     not null varchar(400)

 NAME                      NULL?    TYPE
 ----------------------------------------- -------- ----------------------------
 hostname                  not null varchar(100)
 hw                        not null varchar(32)

sql> desc kickstart
 NAME                      NULL?    TYPE
 ----------------------------------------- -------- ----------------------------
 hostname                  not null varchar(100)
 ks                        text
 source                    not null varchar(500)
 lastupdated               timestamp
 username                  varchar(32)

sql> desc pxeboot;
 NAME                      NULL?    TYPE
 ----------------------------------------- -------- ----------------------------
 name                      not null varchar(100)
 arch                      varchar(12)
 description               varchar(500)
 owner                     varchar(100)
 initrd_source             varchar(500)
 initrd_sum                varchar(32)
 initrd_size               numeric(10)
 vmlinuz_source            varchar(500)
 vmlinuz_sum               varchar(32)
 vmlinuz_size              numeric(10)
 kopts                     varchar(500)
 uploaded                  timestamp
 groups                    varchar(100)
 uefi                      numeric(1)

sql> desc pxehosts;
 NAME                      NULL?    TYPE
 ----------------------------------------- -------- ----------------------------
 hostname                  not null varchar(100)
 status                    not null numeric(1)
 target                    not null varchar(100)
 username                  not null varchar(100)
 kopts                     varchar(500)
 registered                timestamp
 enabled                   timestamp
 booted                    timestamp
 disabled                  timestamp
 noexpiry                  numeric(38)
 type                      numeric(1)
```

## Required DB configuration

`conf` table should have at least the following configured keys:

```
[root@aims01 ~]# aims2config list
EGROUP_UPLOAD => aims2-upload
EGROUP_AIMSSUPPORT => aims-admins
EGROUP_SYSADMINS => aims2-cc-admins
```

You can use `aims2config create <key> <value>` for that matter if you want to avoid updating the DB by hand. You can use `aims2config update <key> <value>` to replace current values.
