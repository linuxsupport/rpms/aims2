##############################################
package aims2image;
##############################################

##############################################
#
# image.pm - PXE boot images handler
#
# Authors:
#         Daniel Juarez Gonzalez <djuarezg@cern.ch>
#         Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#         Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use Digest::MD5 qw(md5_hex);
# Temp logging for https://its.cern.ch/jira/browse/LOS-688
use DBI::Log timing => 1;

1;

##############################################
sub new
##############################################
{
    my ( $class, $dbh, $user, $conf ) = @_;
    my ($self) = bless {
        _DB   => $dbh,
        _USER => $user,
        _CONF => $conf,
    }, $class;

    return $self;
}

##############################################
sub add
##############################################
{
    my ( $self, $image ) = @_;

    my $uefi = 0;

    # Add new pxeboot media to aims2.
    if ( defined( $image->{UEFI} ) ) {
        $uefi = 1;
    }

    my %record = ();

    $record{NAME}         = $image->{NAME};
    $record{OWNER}        = $self->{_USER};
    $record{ARCH}         = $image->{ARCH};
    $record{DESC}         = $image->{DESC};
    $record{KOPTS}        = $image->{KOPTS};
    $record{GROUPS}       = $image->{GROUPS};
    $record{VMLINUZ}      = $image->{VMLINUZ};
    $record{VMLINUZ_SUM}  = $image->{VMLINUZ_SUM};
    $record{VMLINUZ_SIZE} = $image->{VMLINUZ_SIZE};
    $record{INITRD}       = undef;
    $record{INITRD_SUM}   = undef;
    $record{INITRD_SIZE}  = undef;
    $record{UEFI}         = $uefi;

    if ( defined( $image->{INITRD} ) ) {
        $record{INITRD}      = $image->{INITRD};
        $record{INITRD_SUM}  = $image->{INITRD_SUM};
        $record{INITRD_SIZE} = $image->{INITRD_SIZE};
    }

    $self->insert_pxeboot_record(%record);

    $self;
}

##############################################
sub insert_pxeboot_record
##############################################
{
    my ( $self, %record ) = @_;

    my $dbh = $self->{_DB};

    # FIXME: Better bind behaviour.

    # Prepare to talk to the database.
    my @fields =
      qw(name owner arch description kopts groups vmlinuz_source vmlinuz_sum vmlinuz_size initrd_source initrd_sum initrd_size uploaded uefi);
    my $fields = join( ",", @fields );

    my $sql = "INSERT INTO pxeboot ($fields) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?)";

    # Create a new record
    eval {
        my $prepare = $dbh->prepare($sql);

        $prepare->bind_param( 1, $record{NAME} );
        $prepare->bind_param( 2, $record{OWNER} );
        $prepare->bind_param( 3, $record{ARCH} );
        $prepare->bind_param( 4, $record{DESC} );
        $prepare->bind_param( 5, $record{KOPTS} );
        $prepare->bind_param( 6, $record{GROUPS} );
        $prepare->bind_param( 7, $record{VMLINUZ} );
        $prepare->bind_param( 8,  $record{VMLINUZ_SUM} );
        $prepare->bind_param( 9, $record{VMLINUZ_SIZE} );
        $prepare->bind_param( 10, $record{INITRD} );
        $prepare->bind_param( 11, $record{INITRD_SUM} );
        $prepare->bind_param( 12, $record{INITRD_SIZE} );
        $prepare->bind_param( 13, $record{UEFI} );

        $prepare->execute();
    };
    if ($@) {

        # Image already exists.
        if ( $@ =~ /.BOOT_PK/ ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/insert_pxeboot_record] Error: Image "
                  . $record{NAME}
                  . " already exists." );
            die "Error: Image " . $record{NAME} . " already exists.\n";
        }

        # We just blew our quota :(
        if ( $@ =~ /quota exceeded/ ) {
            $dbh->rollback;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/insert_pxeboot_record] Error: Database quota exceeded. Contact Linux.Support\@CERN.CH"
            );
            die "Error: Database quota exceeded. Contact Linux.Support\@CERN.CH\n";
        }

        # Something unexpected.
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/insert_pxeboot_record] Database Error: Failed to create pxeboot record $record{NAME}.\n"
                  . $dbh->errstr );
            die "Database Error: Failed to create pxeboot record $record{NAME}.\n" . $dbh->errstr;
        }
    }

    $self;
}

##############################################
sub remove
##############################################
{
    my ( $self, $name ) = @_;

    # Remove an image from aims2.
    unless ($name) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/remove] Error: Cannot remove image. Name is undefined."
        );
        die "Error: Cannot remove. Name is undefined.\n";
    }

    $name =~ tr/[a-z]/[A-Z]/;
    $self->remove_pxeboot_record($name);

    $self;
}

##############################################
sub remove_pxeboot_record
##############################################
{
    my ( $self, $name ) = @_;

    # Remove pxeboot record from the database.

    my $dbh = $self->{_DB};

    eval {
        my $prepare = $dbh->prepare("DELETE FROM pxeboot WHERE name = ?");
        $prepare->execute($name);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/remove_pxeboot_record] Database Error: Failed to remove $name."
            );
            die "Database Error: Failed to remove $name.\n";
        }
    }

    $self;
}

##############################################
sub get_pxeboot_by_name
##############################################
{
    my ( $self, $name, $arch, $uefi ) = @_;
    # Get pxeboot information matching $name.

    unless ($name) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/get_pxeboot_by_name] Error: Cannot query. NAME is undefined."
        );
        die "Error: Cannot query. NAME is undefined.\n";
    }
    $name =~ tr/*/%/;

    my $dbh = $self->{_DB};

    my @fields =
      qw(name owner arch kopts groups description initrd_source initrd_sum initrd_size vmlinuz_source vmlinuz_sum vmlinuz_size uefi);
    push( @fields, "to_char(uploaded,'yyyy/mm/dd hh24:mi:ss')" );
    my $fields = join( ", ", @fields );
    my $sql    = "SELECT $fields FROM pxeboot WHERE name LIKE ?";
    my @sqlargs;
    my %pxeboot = ();

    push( @sqlargs, $name );

    if ( defined($arch) ) {
        $sql .= " AND arch LIKE ?";
        push( @sqlargs, $arch );
    }

    if ( defined($uefi) ) {
        $sql .= " AND uefi = 1";
    }

    eval {
        my $prepare = $dbh->prepare($sql);

        $prepare->execute(@sqlargs);

        #$prepare->bind_columns(undef, map { \$pxeboot{$_} } @fields);
        while ( my ( $name, @image ) = $prepare->fetchrow_array ) {
            (
                $pxeboot{$name}{OWNER},
                $pxeboot{$name}{ARCH},
                $pxeboot{$name}{KOPTS},
                $pxeboot{$name}{GROUPS},
                $pxeboot{$name}{DESCRIPTION},
                $pxeboot{$name}{INITRD_SOURCE},
                $pxeboot{$name}{INITRD_SUM},
                $pxeboot{$name}{INITRD_SIZE},
                $pxeboot{$name}{VMLINUZ_SOURCE},
                $pxeboot{$name}{VMLINUZ_SUM},
                $pxeboot{$name}{VMLINUZ_SIZE},
                $pxeboot{$name}{UEFI},
                $pxeboot{$name}{UPLOADED},

            ) = @image;

            # TO BE REMOVED LOS-693
            # We fake syns status as it is no longer relevant but old clients use it.
            $pxeboot{$name}{SYNCED1} = "Y";
            $pxeboot{$name}{SYNCED2} = "Y";
            $pxeboot{$name}{SYNCED3} = "Y";
        }
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/get_pxeboot_by_name] Database Error: Cannot retrieve pxeboot information for $name. $errormsg"
            );
            die
"Database Error: Cannot retrieve pxeboot information for $name. $errormsg\n";
        }
    }

    \%pxeboot;
}

##############################################
sub permission
##############################################
{
    my ( $self, $owner, $groups ) = @_;

    $self->SetLDAP;

    # Deny by default
    my $permission = 0;

    # User is owner?
    if ( lc( $self->{_USER} ) eq lc($owner) ) {
        $permission++;
    }

    # User is member of groups ?
    if ( $permission == 0 ) {
        my @tmpgroups = split( ',', $groups );
        foreach my $group (@tmpgroups) {
            if ( $self->memberof($group) ) {
                $permission++;
            }
        }
    }

    # User is member of # User is member of EGROUP_AIMSSUPPORT
    if ( $permission == 0 ) {

        # FIXME: conf.
        if ( $self->memberof( $self->{_CONF}->{'EGROUP_AIMSSUPPORT'} ) ) {
            $permission++;
        }
    }

    if ( $permission == 0 ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/permission] Error: Cannot remove image. Permission denied."
        );
        die "Error: Cannot remove image. Permission denied.\n";
    }

    $self;
}

##############################################
sub upload_permission
##############################################
{
    my ($self) = @_;

    # Check user has permission to upload.

    $self->SetLDAP;

    # Deny by default
    my $permission = 0;

    # User is member of linux support
    if ( $permission == 0 ) {

        # FIXME: conf.
        if ( $self->memberof( $self->{_CONF}->{'EGROUP_AIMSSUPPORT'} ) ) {
            $permission++;
        }
    }

    # User is member of upload list.
    if ( $permission == 0 ) {

        # FIXME: conf.
        if ( $self->memberof( $self->{_CONF}->{'EGROUP_UPLOAD'} ) ) {
            $permission++;
        }
    }

    if ( $permission == 0 ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [image/upload_permission] Error: You do not have permission to upload pxeboot media."
        );
        die "Error: You do not have permission to upload pxeboot media.\n";
    }

    $self;
}

##############################################
sub SetLDAP
##############################################
{
    my ($self) = @_;

    # Connect to ldap (cerndc.cern.ch).
    # Set connection.
    my $ldapserver =
        $self->{_CONF}->{'LDAP_SERVER'}
      ? $self->{_CONF}->{'LDAP_SERVER'}
      : "cerndc.cern.ch";
    my $ldapuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
    my $ldappass =
        $aims2conf::aims2_pass
      ? $aims2conf::aims2_pass
      : die "password undefined";
    my $ldapbase =
        $self->{_CONF}->{'LDAP_BASE'}
      ? $self->{_CONF}->{'LDAP_BASE'}
      : "DC=cern,DC=ch";

    my $ldap = aims2ldap->new();
    $ldap->ldap_connect( $ldapserver, $ldapuser, $ldappass );

    $self->{_LDAP} = $ldap;

}

##############################################
sub memberof
##############################################
{
    my ( $self, $group ) = @_;

    my $member = 0;

    my $ldap = $self->{_LDAP};

    my $rest = '';

    my $ldapbase =
        $self->{_CONF}->{'LDAP_BASE'}
      ? $self->{_CONF}->{'LDAP_BASE'}
      : "DC=cern,DC=ch";

    if ( length($group) != 0 ) {

        ( $group, $rest ) = split( '@', $group );

        # To get a recursive search, or to have AD check relations, extra properties need to be included to the filter.
        # In this case, the string 1.2.840.113556.1.4.1941 will need to be added. According to Microsoft:
        # The string 1.2.840.113556.1.4.1941 specifies LDAP_MATCHING_RULE_IN_CHAIN. This applies only to DN attributes.
        # This is an extended match operator that walks the chain of ancestry in objects all the way to the root until it finds a match.
        # This reveals group nesting. It is available only on domain controllers with Windows Server 2003 SP2 or Windows Server 2008 (or above).
        my $result = $ldap->ldap_search(
            $ldapbase,
"(&(cn=$self->{_USER})(memberOf:1.2.840.113556.1.4.1941:=CN=$group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))",
            ('cn')
        );
        my @result = $ldap->fetch_values( $result, ('cn') );

        for my $user (@result) {
            $user =~ s/^CN=//;
            if ( $self->{_USER} eq $user ) {
                $member = 1;
                last;
            }
        }
    }

    $member;
}
