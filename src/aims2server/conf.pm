##############################################
package aims2conf;
##############################################

##############################################
#
# conf.pm - conf handlers. Used by server
# modules and by aims2config management tool
# Authors: Daniel Juarez <djuarezg@cern.ch>
#          Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#          Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use FileHandle;
# Need to specify the dependency here as aims2config
# uses this module and can't infere the module 
# location as aims2server does
use aims2server::helpers;

1;

##############################################
# Static Database Connection Values
##############################################
our ( $db_conn_string, $db_user, $db_pass,
    $aims2_server, $aims2_user, $aims2_pass )
  = &read_conf;
our %db_conn_attrib = (
    'InactiveDestroy' => 1,
    'LongReadLen'     => 1_000_000_000,
    'AutoCommit'      => 1,
    'RaiseError'      => 1,
    'PrintError'      => 1,
    'LongTruncOk'     => 0
);

##############################################
sub new
##############################################
{
    my ( $class, $dbh ) = @_;
    my ($self) = bless {}, $class;
    $self->{_DB} = $dbh;
    return $self;
}

##############################################
sub read_conf
##############################################
{
    my $conf = new FileHandle;

    # We will only have this conf file and its credentials on the puppet node as this comes from Teigi, so no need to use auth/authz
    $conf->open("/etc/aims2.conf") or die "Error: Missing configuration. $!\n";
    my %dbconf = ();
    while ( <$conf> ) {
        chomp;                  # no newline
        s/#.*//;                # no comments
        s/^\s+//;               # no leading white
        s/\s+$//;               # no trailing white
        next unless length;     # anything left?
        my ($key, $val) = $_ =~ /(.*?)=(.*)/;
        $dbconf{$key} = $val;
    }
    $conf->close();
    (
        $dbconf{DB}, $dbconf{User}, $dbconf{Pass},
        $dbconf{AimsServer}, $dbconf{AimsUser}, $dbconf{AimsPass}
    );
}

##############################################
sub get
##############################################
{
    my ($self) = @_;
    my ( %conf, $key, $value );
    my $dbh = $self->{_DB};
    my $sql = "SELECT key, value FROM conf";
    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->execute();
        $prepare->bind_columns( undef, \$key, \$value );
        while ( $prepare->fetch() ) {
            $conf{$key} = $value;
        }
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [conf/get] Database Error: Cannot get server configuration."
            );
            die "Database Error: Cannot get server configuration.\n";
        }
    }

    \%conf;
}

##############################################
sub get_value
##############################################
{
    my ( $self, $key ) = @_;

    unless ($key) {
        aims2helpers::alog(
"[conf/get_value] Error: Configuration key required."
        );
        die "Error: Configuration key required.\n";
    }

    $key =~ tr/[a-z]/[A-Z]/;

    my $conf  = $self->get();
    my $value = %$conf{$key};
    $value || undef;
}

##############################################
sub update
##############################################
{
    my ( $self, $key, $value ) = @_;
    unless ( $key && $value ) {
        aims2helpers::alog(
"[conf/update] Error: Configuration update requires key and value."
        );
        die "Error: Configuration update requires key and value.\n";
    }
    my $dbh = $self->{_DB};
    $key =~ tr/[a-z]/[A-Z]/;
    $key =~ tr/ /_/;
    eval {
        my $prepare =
          $dbh->prepare("UPDATE conf SET value=? WHERE key = ?");
        $prepare->bind_param( 1, $value );
        $prepare->bind_param( 2, $key );
        $prepare->execute;
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"[conf/update] Database Error: Unable to update configuration value."
            );
            die "Database Error: Unable to update configuration value.\n";
        }
    }
    aims2helpers::alog(
        "[conf/update] Updating key $key with $value value"
    );
    $self;

}

##############################################
sub create
##############################################
{
    my ( $self, $key, $value ) = @_;
    unless ( $key && $value ) {
        aims2helpers::alog(
"[conf/create] Error: Configuration create requires key and value."
        );
        die "Error: Configuration create requires key and value.\n";
    }
    chomp($key);
    chomp($value);
    $key =~ tr/[a-z]/[A-Z]/;
    $key =~ tr/ /_/;
    my $dbh = $self->{_DB};
    eval {
        my $prepare = $dbh->prepare(
            "INSERT INTO conf (key, value) VALUES (?, ?)");
        $prepare->bind_param( 1, $key );
        $prepare->bind_param( 2, $value );
        $prepare->execute;
    };
    if ($@) {

        # Is the key a duplicate?
        if ( $@ =~ /.CONF_PK/ ) {
            aims2helpers::alog(
"[conf/create] Database Error: Key with that name already exists."
            );
            die "Database Error: Key with that name already exists.\n";
        }
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"[conf/create] Database Error: Failed to create new configuration key/value."
            );
            die
              "Database Error: Failed to create new configuration key/value.\n";
        }
    }
    aims2helpers::alog(
        "[conf/update] Creating key $key with $value value."
    );
    $self;
}

##############################################
sub remove
##############################################
{
    my ( $self, $key ) = @_;
    unless ($key) {
        aims2helpers::alog(
"[conf/remove] Error: Configuration removal requires key and value."
        );
        die "Error: Configuration removal requires key and value.\n";
    }
    my $dbh = $self->{_DB};
    $key =~ tr/[a-z]/[A-Z]/;

    eval {
        my $prepare = $dbh->prepare("DELETE FROM conf WHERE key = ?");
        $prepare->bind_param( 1, $key );
        $prepare->execute;
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"[conf/remove] Database Error: Failed to remove configuration value $key."
            );
            die "Database Error: Failed to remove configuration value $key.\n";
        }
    }
    aims2helpers::alog("[conf/remove] Deleting key $key.");
    $self;
}

