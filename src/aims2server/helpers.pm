##############################################
package aims2helpers;
##############################################

##############################################
#
# helpers.pm - aims2 helpers shared
# across all different modules
# Authors:
#    Daniel Juarez Gonzalez <djuarezg@cern.ch>
#
##############################################

use strict;
use POSIX qw(setsid strftime);
use Sys::Syslog qw(:standard :macros);
use File::Path;
use IO::File;
use Digest::MD5;
use Date::Manip;
use Date::Parse;
use DBI;
use File::Find::Rule;
use File::stat;
use Time::Local;
use File::Copy;
use aims2server::db;

# Temp logging for https://its.cern.ch/jira/browse/LOS-688
use DBI::Log timing => 1;


my $TFTP_ROOT        = "/aims_share/tftpboot/aims";
my $TMP_UPLOAD       = "/aims_share/tftpboot/aimshttpupload";
my $TFTP_CONF_BIOS   = "/config/bios";
my $TFTP_CONF_UEFI   = "/config/x86_64-efi";
my $TFTP_CONF_ARM64  = "/config/arm64-efi";
my $DNSMASQ_CONF_DIR = "/aims_share/dnsmasq";
my $TFTP_BOOT        = "/boot";
my $SERVER =
  $aims2conf::aims2_server ? $aims2conf::aims2_server : "aims.cern.ch";
my $KSURL = "http://" . $SERVER . "/aims/ks";

# This may be called from whithin the service and not through any request
# so we set it to this value otherwise
$ENV{REMOTE_ADDR} = $ENV{REMOTE_ADDR} ? $ENV{REMOTE_ADDR} : "localhost";


1;

##############################################
sub new
##############################################
{
    my ( $class, $dbh, $user, $conf ) = @_;
    my ($self) = bless {
        _DB   => $dbh,
        _USER => $user,
        _CONF => $conf,
    }, $class;

    return $self;
}

##############################################
sub remove_config
##############################################
{
    my ( $interface, $config, $what ) = @_;
    my $intf    = "01-" . lc($interface);
    my $cfgfile = $TFTP_ROOT . $TFTP_CONF_BIOS . "/$intf";
    # Interfaces are prepended with 01 before the MAC
    my $intf2          = "grub.cfg-" . $intf;
    my $need_to_update = 0;

    if ( $what != 0 ) {
        $need_to_update +=
          remove_config_2( $cfgfile, $config->{HOSTNAME}, $intf, "bios" );
    }
    if ( $what != 1 ) {
        $cfgfile = $TFTP_ROOT . $TFTP_CONF_UEFI . "/$intf2";
        $need_to_update +=
          remove_config_2( $cfgfile, $config->{HOSTNAME}, $intf, "uefi" );
    }
    if ( $what != 2 ) {
        $cfgfile = $TFTP_ROOT . $TFTP_CONF_ARM64 . "/$intf2";
        $need_to_update +=
          remove_config_2( $cfgfile, $config->{HOSTNAME}, $intf, "arm64" );
    }

    return $need_to_update;
}

##############################################
sub remove_config_2
##############################################
{
    my ( $cfgfile, $hostname, $intf, $what ) = @_;

    if ( -e $cfgfile ) {
        alog("$ENV{REMOTE_ADDR} - REM pxe conf for $intf ($hostname) [$what]");
        unlink $cfgfile or groan("cannot remove $cfgfile: $!");
        return 1;
    }
    return 0;
}

##############################################
sub create_hostname_pxeconf
##############################################
{
    my ( $self, $hostname ) = @_;

    # Now that we have all iface info, build confs for each network iface the device has
    # Note this is only done when enabling pxe, not when adding a device to AIMS2
    my $configs = get_pxehost( $self->{_DB}, $hostname );
    foreach my $interface ( keys %$configs ) {
        if ( defined( $configs->{$interface}{STATUS} )
            && $configs->{$interface}{STATUS} == 1 )
        {
            # Some important checks.
            # TARGET
            unless ( $configs->{$interface}{TARGET} ) {
                alog(
"$ENV{REMOTE_ADDR} - No TARGET defined for $configs->{$interface}{HOSTNAME}, skipping $interface."
                );
                next;
            }

            # ARCH
            unless ( $configs->{$interface}{ARCH} ) {
                alog(
"$ENV{REMOTE_ADDR} - No ARCH defined for $configs->{$interface}{HOSTNAME}, skipping $interface."
                );
                next;
            }

            # Condense anaconda options to be printed in the pxe conf
            $configs->{$interface}->{KOPTS} = condense_kopts(
                $configs->{$interface}->{HOSTKOPTS},
                $configs->{$interface}->{TARGETKOPTS}
            );
            delete $configs->{$interface}->{HOSTKOPTS};
            delete $configs->{$interface}->{TARGETKOPTS};

            # BOOTLOADER configuration writing to TFTP paths
            if ( grep { $configs->{$interface}->{ARCH} eq $_ }
                qw(i386 x86_64 i686 aarch64) )
            {
                build_bootloader( $interface, $configs->{$interface} );
                next;
            }
        }
    }
}

##############################################
sub delete_hostname_pxeconf
##############################################
{
    my ( $self, $hostname ) = @_;

    # Now that we have all iface info, build confs for each network iface the device has
    # Note this is only done when enabling pxe, not when adding a device to AIMS2
    my $configs = get_pxehost( $self->{_DB}, $hostname );
    foreach my $interface ( keys %$configs ) {
        remove_config( $interface, $configs->{$interface}, -1 );
    }
}

##############################################
sub build_bootloader
##############################################
{
    my ( $interface, $config ) = @_;
    $interface =~ tr/[A-Z]/[a-z]/;
    my $type = $config->{TYPE};
    my $file = "$TFTP_ROOT";
    $type = 0 if ( !defined($type) );

    if ( $type == 0 ) {
        $file = $file . $TFTP_CONF_BIOS . "/01-$interface";
        build_pxelinux( $file, $interface, $config );
        remove_config( $interface, $config, 0 );
    }
    elsif ( $type == 1 ) {
        $file = $file . $TFTP_CONF_UEFI . "/grub.cfg-01-$interface";
        build_grub( $file, $interface, $config, "uefi" );
        remove_config( $interface, $config, 1 );
    }
    else {
        $file = $file . $TFTP_CONF_ARM64 . "/grub.cfg-01-$interface";
        build_grub( $file, $interface, $config, "arm64" );
        remove_config( $interface, $config, 2 );
    }
}

##############################################
sub build_pxelinux
##############################################
{
    my ( $cfgfile, $interface, $config ) = @_;
    my $prefix   = "http://" . $SERVER;
    my $boottype = "bios";
    my $build = IO::File->new("> $cfgfile") or die "Can't open new file: $!";
    if ( defined $build ) {
        print $build "default linux\n label linux\n";
        print $build " kernel $prefix/aims/boot/"
          . $config->{TARGET}
          . "/vmlinuz\n";
        my $append = " append";

        # Is there an initrd defined?
        if ( $config->{INITRD} ) {
            $append =
                $append
              . " initrd=$prefix/aims/boot/"
              . $config->{TARGET}
              . "/initrd";
        }
        $append = $append . " " . $config->{KOPTS} . " inst.ks=$KSURL\n";
        print $build $append;
        print $build " ipappend 2\n";

        $build->close;
    }

    # Replace all "-" with ":" so all components log MAC addresses in the same format. Constrained by dnsmasq unmutable format
    $interface =~ s/-/:/ig;
    # Can't do this in one line
    my $macified_interface = $interface;
    # Chop the first characters '01' from the ARP type
    $macified_interface =~ s/^01//;

    alog(
"$ENV{REMOTE_ADDR} - ADD pxe conf for 01-$interface / MAC $macified_interface ($config->{HOSTNAME}) [$boottype]"
    );
}

##############################################
sub build_grub
##############################################
{
    my ( $cfgfile, $interface, $config, $what ) = @_;

    my $build = IO::File->new("> $cfgfile") or die "Can't open new file: $!";

    if ( defined $build ) {

        print $build "set default='linux'\n";
        print $build "set timeout=0\n";
        if ( defined( $config->{ARCH} )
            && $config->{ARCH} eq "x86_64" )
        {
            $what = "uefi";

            # LOS-668: Do not use progress mod for arm64 or it gets stuck when loading kernel
            print $build "insmod progress\n";
        }
        print $build "menuentry 'linux' {\n";
        print $build "clear\n";
        print $build "echo -n 'loading vmlinuz ...'\n";

        # Always use http instead of tftp to speed up client downloads
        # LOS-763: Temporary workaround, removing (http) method as some nodes timeout due to slow download speeds. Only for UEFI! ARM64 should still use it to avoid timeouts
        if ( defined( $config->{ARCH} )
            && $config->{ARCH} eq "aarch64" )
        {
            $what = "arm64";
            print $build "linux (http)/aims/boot/" . $config->{TARGET} . "/vmlinuz";
        }
        else{
            print $build "linux /aims/boot/" . $config->{TARGET} . "/vmlinuz";
        }

        # Kernel boot parameters:
        # - append Kickstart, its KSURL endpoint will always return the KS file a node has associated in AIMS
        # - ip=dhcp: This parameter tells the kernel how to configure IP addresses of devices
        # and also how to set up the IP routing table
        # - we also append any option the user might have added for their specific case
        my $append = " ip=dhcp inst.ks=$KSURL " . $config->{KOPTS};
        print $build $append . "\n";

        if ( $config->{INITRD} ) {
            print $build "clear\n";
            print $build "echo -n 'loading initrd ...'\n";
            # LOS-763: Temporary workaround, removing (http) method as some nodes timeout due to slow download speeds. Only for UEFI! ARM64 should still use it to avoid timeouts
            if ( defined( $config->{ARCH} )
            && $config->{ARCH} eq "aarch64" )
            {
               print $build "initrd (http)/aims/boot/" . $config->{TARGET} . "/initrd\n";
            }
            else {
                print $build "initrd /aims/boot/" . $config->{TARGET} . "/initrd\n";
            }
        }
        print $build "}\n";
        $build->close;
    }

    # Replace all "-" with ":" so all components log MAC addresses in the same format. Constrained by dnsmasq unmutable format
    $interface =~ s/-/:/ig;
    # Can't do this in one line
    my $macified_interface = $interface;
    # Chop the first characters '01' from the ARP type
    $macified_interface =~ s/^01//;

    alog(
"$ENV{REMOTE_ADDR} - ADD pxe conf for 01-$interface / MAC $macified_interface ($config->{HOSTNAME}) [$what]"
    );
}

# --------------------------------------------
# Internal helpers
# --------------------------------------------

##############################################
sub insert0 {
##############################################
    # 7th July =~ 07th July :)
    my ($date) = shift;
    if ( $date < 10 ) {
        return "0$date";
    }
    return $date;
}

##############################################
sub getdate {
##############################################
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $iddst ) =
      localtime(time);
    my $datestring;
    $year += 1900;
    $mon++;
    $mon        = insert0($mon);
    $mday       = insert0($mday);
    $min        = insert0($min);
    $datestring = "$year-$mon-$mday $hour:$min";
    return ($datestring);
}

##############################################
sub alog {
##############################################
    my ($status) = @_;

    syslog( "info", $status . "\n" );
}

##############################################
sub get_pxehost {
##############################################

    my ( $dbh, $hostname ) = @_;

    my %configs = ();

    # Define the SQL query.
    my $sql = <<EOSQL;
SELECT
  i.hw, h.hostname, h.status, t.arch, t.name, h.kopts, t.kopts, t.vmlinuz_source, t.initrd_source,
  to_char(h.registered, 'yyyy/mm/dd hh24:mi:ss'), to_char(h.enabled, 'yyyy/mm/dd hh24:mi:ss'),
  to_char(h.disabled, 'yyyy/mm/dd hh24:mi:ss'), h.noexpiry, h.type
FROM
  hardware i,
  pxehosts h
LEFT OUTER JOIN
  pxeboot t
ON
  h.target = t.name
WHERE (h.hostname = ? AND i.hostname = ?)
EOSQL

    # Prepare to talk to the database.
    # Return ref(%config);
    eval {
        my $prepare = $dbh->prepare($sql)
          or groan( "warning: failed to prepare host query." . $dbh->errstr );
        $prepare->bind_param( 1, $hostname );
        $prepare->bind_param( 2, $hostname );
        $prepare->execute();

        while ( my ( $hw, @config ) = $prepare->fetchrow_array ) {
            (
                $configs{$hw}{HOSTNAME},  $configs{$hw}{STATUS},
                $configs{$hw}{ARCH},      $configs{$hw}{TARGET},
                $configs{$hw}{HOSTKOPTS}, $configs{$hw}{TARGETKOPTS},
                $configs{$hw}{VMLINUZ},   $configs{$hw}{INITRD},
                $configs{$hw}{REG},       $configs{$hw}{ON},
                $configs{$hw}{OFF},       $configs{$hw}{NOEXPIRY},
                $configs{$hw}{TYPE},
            ) = @config;
        }
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            groan("warning: failed to query for host changes. see logs.");
            alog("Query failed: $@");
        }
    }
    \%configs;
}

##############################################
sub condense_kopts
##############################################
{
    # Condense PXEBOOT and PXEHOOST KOPTS into one string. Remove duplicates.
    my ( $host, $image ) = @_;
    $host  = "" if ( !$host );
    $image = "" if ( !$image );
    my @kopts  = ( split( " ", $image ), split( " ", $host ) );
    my %repeat = ();
    @kopts = grep { !$repeat{$_}++ } @kopts;
    my $kopts = "";

    foreach (@kopts) {
        $kopts = $kopts . $_ . " ";
    }
    $kopts;
}

##############################################
sub groan
##############################################
{
    my $groan = shift;
    alog("error: $groan");
    die "There was a problem while processing your action, please contact the administrators."
}
