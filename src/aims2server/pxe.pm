##############################################
package aims2pxe;
##############################################

##############################################
#
# pxe.pm - aims2 pxe controller. Pxe conf builder
#
# Authors:
#    Daniel Juarez Gonzalez <djuarezg@cern.ch>
#
##############################################

use strict;
use aims2server::helpers;

# Temp logging for https://its.cern.ch/jira/browse/LOS-688
use DBI::Log timing => 1;

1;

##############################################
sub new
##############################################
{
    my ( $class, $dbh, $user, $conf ) = @_;
    my ($self) = bless {
        _DB   => $dbh,
        _USER => $user,
        _CONF => $conf,
    }, $class;

    return $self;
}

##############################################
sub enable
##############################################
{
    my ( $self, $hostname, $pxeboot, $forever, $boottype ) = @_;

    # Enable the target host for installation with pxeboot

    my $noexpiry = "noexpiry=0";
    my $type     = "type=$boottype";

    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/enable] Error: No target hostname specified."
        );
        die "Error: No target hostname specified.\n";
    }
    $hostname =~ tr/[a-z]/[A-Z]/;
    unless ($pxeboot) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/enable] Error: No pxeboot target specified for $hostname"
        );
        die "Error: No pxeboot target specified.\n";
    }
    $hostname =~ tr/[a-z]/[A-Z]/;
    if ($forever) { $noexpiry = "noexpiry=1"; }

    my $dbh = $self->{_DB};
    my $sql =
"UPDATE pxehosts SET target=(SELECT name FROM pxeboot WHERE name = ?),status=1,enabled=CURRENT_TIMESTAMP,disabled=null,booted=null,"
      . $noexpiry . ","
      . $type
      . " WHERE hostname = ?";
    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->execute( $pxeboot, $hostname );
    };
    if ($@) {
        if ( $@ =~ /TARGET/ ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/enable] Error: $pxeboot target does not exist. $hostname tried using it."
            );
            die "Error: $pxeboot target does not exist.\n";
        }
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/enable] Database Error: Failed to enable PXE for $hostname $errormsg."
            );
            die "Database Error: Failed to enable PXE for $hostname $errormsg.\n";
        }
    }

    my $helpers = aims2helpers->new( $dbh, $self->{_USER}, $self->{_CONF} );
    $helpers->create_hostname_pxeconf($hostname);

    $self;

}

##############################################
sub disable
##############################################
{
    my ( $self, $hostname ) = @_;
    # Set the device to localboot.

    unless ($hostname) {
        aims2helpers::alog(
            "$ENV{REMOTE_ADDR} - [pxe/disable] Error: Hostname cannot be null."
        );
        die "Error: Hostname cannot be null.\n";
    }
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $dbh = $self->{_DB};
    eval {
        my $prepare = $dbh->prepare(
"UPDATE pxehosts SET status=2,target='localboot',disabled=CURRENT_TIMESTAMP WHERE hostname = ?"
        );
        $prepare->execute($hostname);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/disable] Database Error: Failed to disable PXE for $hostname $errormsg."
            );
            die "Database Error: Failed to disable PXE for $hostname $errormsg.\n";
        }
    }

    my $helpers = aims2helpers->new( $dbh, $self->{_USER}, $self->{_CONF} );
    $helpers->delete_hostname_pxeconf($hostname);

    $self;
}

##############################################
sub changetobios
##############################################
{
    my ( $self, $host ) = @_;

    unless ($host) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/changetobios] Error: Hostname cannot be null."
        );
        die "Error: Hostname cannot be null.\n";
    }
    $host =~ tr/[a-z]/[A-Z]/;

    my $dbh = $self->{_DB};
    eval {
        my $prepare =
          $dbh->prepare("UPDATE pxehosts SET type=0 WHERE hostname = ?");
        $prepare->execute($host);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [pxe/changetobios] Database Error: Failed to changetobios for $host $errormsg."
            );
            die "Database Error: Failed to changetobios for $host $errormsg.\n";
        }
    }

    $self;
}

