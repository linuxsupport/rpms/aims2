##############################################
package aims2host;
##############################################

##############################################
#
# host.pm - Host info, hw ifaces handler
#
# Authors:
#         Daniel Juarez Gonzalez <djuarezg@cern.ch>
#         Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#         Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use IO::File;
use SQL::Abstract;
use MIME::Base64;
use aims2server::ldap;
use aims2server::db;

# Temp logging for https://its.cern.ch/jira/browse/LOS-688
use DBI::Log timing => 1;

1;

##############################################
sub new
##############################################
{
    my ( $class, $dbh, $user, $conf ) = @_;
    my ($self) = bless {
        _DB   => $dbh,
        _USER => $user,
        _CONF => $conf,
    }, $class;

    return $self;
}

##############################################
sub remove
##############################################
{
    my ( $self, $host ) = @_;

    # Remove a device from aims2.

    unless ($host) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/remove] Error: Cannot remove undefined host."
        );
        die "Error: Cannot remove undefined host.\n";
    }

    $self->remove_host_record($host);

    $self;
}

##############################################
sub get_device_by_name
##############################################
{
    my ( $self, $hostname ) = @_;
    # Get device informaation using hostname.

    my $dbh = $self->{_DB};

    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_device_by_name] Error: No query string provided."
        );
        die "Error: No query string provided.\n";
    }
    $hostname =~ tr/*/%/;
    $hostname =~ tr/[a-z]/[A-Z]/;

    # Select device information from database.
    # REGISTERERED ENABLED BOOTED DISABLED
    my @fields =
      qw(i.hw h.hostname h.status h.target h.username h.kopts h.noexpiry h.type);
    push( @fields, "to_char(h.registered,'yyyy/mm/dd hh24:mi:ss')" );
    push( @fields, "to_char(h.enabled,'yyyy/mm/dd hh24:mi:ss')" );
    push( @fields, "to_char(h.disabled,'yyyy/mm/dd hh24:mi:ss')" );
    push( @fields, "to_char(h.booted,'yyyy/mm/dd hh24:mi:ss')" );
    my $fields = join( ",", @fields );
    my $sql = "SELECT $fields FROM pxehosts h, hardware i WHERE h.hostname LIKE ? and i.hostname = h.hostname";

    # Like queries are slow. Displaying all host will still take time,
    # but the database work is over in less than a second.
    # (it's transporting the big %hosts struct that takes time)
    if ( $sql eq "%" ) {
        $sql =
"SELECT $fields FROM pxehosts h, hardware i WHERE i.hostname = h.hostname";
    }

    my %hosts = ();

    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->execute($hostname);
        $prepare->bind_columns( undef, map { \$_ } @fields );
        while ( my ( $hardware, @host ) = $prepare->fetchrow_array ) {
            (
                $hosts{$hardware}{HOSTNAME}, $hosts{$hardware}{STATUS},
                $hosts{$hardware}{PXEBOOT},  $hosts{$hardware}{USERNAME},
                $hosts{$hardware}{KOPTS},    $hosts{$hardware}{NOEXPIRY},
                $hosts{$hardware}{TYPE},     $hosts{$hardware}{REG},
                $hosts{$hardware}{ENABLED},  $hosts{$hardware}{DISABLED},
                $hosts{$hardware}{BOOTED},
            ) = @host;
        }
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_device_by_name] Database Error: Failed to retrieve host information. $errormsg"
            );
            die "Database Error: Failed to retrieve host information. $errormsg\n";
        }
    }

    \%hosts;

}

##############################################
sub validate_hardware
##############################################
{
    my ( $self, $hostname, @hardware ) = @_;

    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/validate_hardware] Error: hostname not provided."
        );
        die "Error: hostname not provided.\n";
    }

    unless (@hardware) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/validate_hardware] hardware address not provided."
        );
        die "Error: hardware address not provided.\n";
    }

    my $dbh = $self->{_DB};

    eval {
        my @foundhosts;
        for my $hw (@hardware) {
            my $prepare = $dbh->prepare(
                "SELECT hostname FROM hardware WHERE hostname != ? AND hw = ?");
            $prepare->execute( $hostname, $hw );
            while ( my (@foundhost) = $prepare->fetchrow_array ) {
                push( @foundhosts, join( " ", @foundhost ) );
            }
        }
        if (@foundhosts) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/validate_hardware] Hardware address for $hostname already in use by "
                  . join( " ", @foundhosts ) );
            die "Hardware address for $hostname already in use by "
              . join( " ", @foundhosts ) . "\n";
        }
    };

    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/validate_hardware] Error: Failed to validate hardware information:\n$errormsg"
            );
            die "Error: Failed to validate hardware information:\n$errormsg";
        }
    }
}

##############################################
sub get_host_kickstart
##############################################
{
    my ( $self, $host ) = @_;
    # Retrieve host kickstart file (and meta data)

    my $dbh = $self->{_DB};

    unless ($host) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_host_kickstart] Error: No query string provided."
        );
        die "Error: No query string provided.\n";
    }

    # Select device information from database.
    my @fields = qw(hostname ks source username);
    push( @fields, "to_char(lastupdated,'yyyy/mm/dd hh24:mi:ss')" );
    my $fields = join( ",", @fields );
    my $sql    = "SELECT $fields FROM kickstart WHERE hostname = ?";

    my @kickstart;

    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->execute($host);
        $prepare->bind_columns( undef, map { \$_ } @fields );
        @kickstart = $prepare->fetchrow_array;
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_host_kickstart] Database Error: Failed to retrieve kickstart information."
            );
            die "Database Error: Failed to retrieve kickstart information.\n";
        }
    }

    \@kickstart;
}

##############################################
sub add
##############################################
{
    my ( $self, $hostname, $device, $kssource, $kopts ) = @_;
    # Add a device to the aims2 database.

    my $dbh = $self->{_DB};
    my @hardware;

    if ( uc($hostname) eq uc( $device->{'DeviceName'} ) ) {

        # Normal case. hostname = device.
        my $interface = $device->{'NetworkInterfaceCards'};
        for my $i (@$interface) {
            push( @hardware, $i->{HardwareAddress} )
              if ( $i->{HardwareAddress} );
        }
    }
    else {
       # Presume it was an interface alias.
       # found at $device->{INTERFACES}->{BoundInterfaceCard}->{HardwareAddress}
        my $interfaces = $device->{'Interfaces'};
        for my $i (@$interfaces) {
            if ( uc( $i->{'Name'} ) eq uc($hostname) ) {
                push( @hardware, $i->{BoundInterfaceCard}->{HardwareAddress} )
                  if ( $i->{BoundInterfaceCard}->{HardwareAddress} );
            }
        }
    }

    # Get interfaces addresses from $device.
    unless (@hardware) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/add] Error: No interface data returned from LanDB for $hostname."
        );
        die "Error: No interface data returned from LanDB for $hostname.\n";
    }

    my $kickstart = $self->validate_ks($kssource);

    $kopts = $self->validate_kopts($kopts);

    $self->validate_hardware( $hostname, @hardware );

    $self->insert_host_record( $hostname, $kopts );
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/add] Host record for $hostname has been inserted"
    );

    $self->insert_hw_records( $hostname, @hardware );

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/add] Hardware records for $hostname have been inserted"
    );

    if ($kickstart) {
        $self->insert_ks_record( $hostname, $kickstart, $kssource );
        aims2helpers::alog(
    "$ENV{REMOTE_ADDR} - [host/add] Kickstart record for $hostname has been inserted"
        );
    }

    $self;

}

##############################################
sub updateks
##############################################
{
    my ( $self, $hostname, $source ) = @_;

    # Update kickstart file in database. sync or use $kickstart;
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/updateks] Cannot updateks. No hostname provided."
        );
        die "Cannot updateks. No hostname provided.\n";
    }
    $hostname =~ tr/[a-z]/[A-Z]/;

    if ($source) {

        # New kickstart file to get.
        my $kickstart  = $self->validate_ks($source);
        my $ks         = $self->get_host_kickstart($hostname);
        my $registered = @$ks[0];
        if ($registered) {
            $self->update_ks_record( $hostname, $kickstart, $source );
        }
        else {
            $self->insert_ks_record( $hostname, $kickstart, $source );
        }
    }
    else {
        # Sync the old one.
        my $ks  = $self->get_host_kickstart($hostname);
        my $src = @$ks[2];
        unless ($src) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/updateks] Error: No previous kickstart source path is defined for $hostname."
            );
            die
"Error: No previous kickstart source path is defined for $hostname.\n";
        }
        if ( $src eq "Uploaded" ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/updateks] Error: This kickstart file was uploaded directly."
            );
            die "Error: This kickstart file was uploaded directly.\n";
        }
        my $kickstart = $self->validate_ks($src);
        $self->update_ks_record( $hostname, $kickstart, $src );
    }

    $self;
}

##############################################
sub update_ks_record
##############################################
{
    my ( $self, $hostname, $kickstart, $source ) = @_;
    # Update kickstart file for HOSTNAME

    my $dbh = $self->{_DB};

    # Means we have "something"
    my $sql =
"UPDATE kickstart SET ks=?,source=?,lastupdated=CURRENT_TIMESTAMP,username=? WHERE hostname = ?";
    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->bind_param( 1, $kickstart );
        $prepare->bind_param( 2, $source );
        $prepare->bind_param( 3, $self->{_USER} );
        $prepare->bind_param( 4, $hostname );
        $prepare->execute();
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/update_ks_record] Database Error: Cannot update kickstart file for $hostname. $errormsg"
            );
            die
"Database Error: Cannot update kickstart file for $hostname. $errormsg\n";
        }
    }

    $self;

}

##############################################
sub insert_host_record
##############################################
{
    my ( $self, $hostname, $kopts ) = @_;

    my $dbh = $self->{_DB};

    unless ($kopts) {
        $kopts = undef;
    }

    my @fields = qw(hostname status username kopts registered target);
    my $fields = join( ",", @fields );
    my $sql =
"INSERT INTO pxehosts ($fields) VALUES (?,0,?,?,CURRENT_TIMESTAMP,'default')";

    eval {
        my $prepare = $dbh->prepare($sql);
        $prepare->execute( $hostname, $self->{_USER}, $kopts );
    };
    if ($@) {
        if ( $@ =~ /.HOST_PK/ ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_host_record] Error: $hostname is already registered."
            );
            die "Error: $hostname is already registered.\n";
        }
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_host_record] Database Error: Cannot register $hostname $errormsg."
            );
            die "Database Error: Cannot register $hostname $errormsg.\n";
        }
    }

    $self;

}

##############################################
sub insert_hw_records
##############################################
{
    my ( $self, $hostname, @hardware ) = @_;
    # Create hwaddr records.

    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_hw_records] Error: Cannot create hardware entries without hostname."
        );
        die "Error: Cannot create hardware entries without hostname.\n";
    }
    unless (@hardware) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_hw_records] Error: Hardware cannot be null."
        );
        die "Error: Hardware cannot be null.\n";
    }

    my $dbh = $self->{_DB};

    eval {
        for my $hw (@hardware) {
            my $prepare =
              $dbh->prepare("INSERT INTO hardware (hostname, hw) VALUES (?,?)");
            $prepare->execute( $hostname, $hw );
        }
    };
    if ($@) {
        if ( $@ =~ /.HW_PK/ ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_hw_records] Error: Hardware address is already registered to another device."
            );
            die
"Error: Hardware address is already registered to another device.\n";
        }
        if ( $@ =~ $dbh->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_hw_records] Database Error: Cannot register hardware for $hostname."
            );
            die "Database Error: Cannot register hardware for $hostname.\n";
        }
    }

    $self;
}

##############################################
sub insert_ks_record
##############################################
{
    my ( $self, $hostname, $kickstart, $source ) = @_;
    # Insert kickstart to database.

    my $dbh = $self->{_DB};

    if ( defined($kickstart) ) {

        # Means we have "something"
        my $sql = "INSERT INTO kickstart (hostname, ks, source, username, lastupdated) VALUES (?,?,?,?,CURRENT_TIMESTAMP)";
        eval {
            my $prepare = $dbh->prepare($sql);
            $prepare->bind_param( 1, $hostname );
            $prepare->bind_param( 2, $kickstart );
            $prepare->bind_param( 3, $source );
            $prepare->bind_param( 4, $self->{_USER} );
            $prepare->execute();
        };
        if ($@) {
            if ( $@ =~ /quota exceeded/ ) {
                aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_ks_record] Database Error: Cannot create kickstart record. aims quota exceeded. Contact Linux Support."
                );
                die
"Database Error: Cannot create kickstart record. aims quota exceeded. Contact Linux Support.\n";
            }
            if ( $@ =~ $dbh->err ) {
                my $errormsg = $@;
                aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/insert_ks_record] Database Error: Cannot create kickstart record $errormsg."
                );
                die "Database Error: Cannot create kickstart record $errormsg.\n";
            }
        }
    }

    $self;

}

##############################################
sub remove_host_record
##############################################
{
    my ( $self, $hostname ) = @_;
    # Delete host records (include hardware and kickstart).

    my $dbh = $self->{_DB};

    eval {
        my $prepare = $dbh->prepare("DELETE FROM pxehosts WHERE hostname = ?");
        $prepare->execute($hostname);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/remove_host_record] Database Error: Cannot remove $hostname record from pxehosts $errormsg."
            );
            die
"Database Error: Cannot remove $hostname record from pxehosts $errormsg.\n";
        }
    }

    eval {
        my $prepare = $dbh->prepare("DELETE FROM hardware WHERE hostname = ?");
        $prepare->execute($hostname);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/remove_host_record] Database Error: Cannot remove $hostname record from hardware $errormsg."
            );
            die
"Database Error: Cannot remove $hostname record from hardware $errormsg.\n";
        }
    }

    eval {
        my $prepare = $dbh->prepare("DELETE FROM kickstart WHERE hostname = ?");
        $prepare->execute($hostname);
    };
    if ($@) {
        if ( $@ =~ $dbh->err ) {
            my $errormsg = $@;
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/remove_host_record] Database Error: Cannot remove $hostname record from kickstart $errormsg."
            );
            die
"Database Error: Cannot remove $hostname record from kickstart $errormsg.\n";
        }
    }

    $self;
}

##############################################
sub permission
##############################################
{
    my ( $self, $device ) = @_;

    my ( $mainuser, $responsible, $landbmanager ) = $self->GetOwners($device);

    # Connect to ldap (cerndc.cern.ch).
    my $ldapserver =
        $self->{_CONF}->{'LDAP_SERVER'}
      ? $self->{_CONF}->{'LDAP_SERVER'}
      : "cerndc.cern.ch";
    my $ldapuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
    my $ldappass =
        $aims2conf::aims2_pass
      ? $aims2conf::aims2_pass
      : die "password undefined";
    my $ldapbase =
        $self->{_CONF}->{'LDAP_BASE'}
      ? $self->{_CONF}->{'LDAP_BASE'}
      : "DC=cern,DC=ch";

    # Use LDAP to get CNs for mail addresses.
    my $ldap = aims2ldap->new();
    $ldap->ldap_connect( $ldapserver, $ldapuser, $ldappass );
    my $result = $ldap->ldap_search( $ldapbase,
        "|(mail=$mainuser)(mail=$responsible)(mail=$landbmanager)", ('cn') );
    my @ldapusers = $ldap->fetch_values( $result, ('cn') );

    $self->{_LDAP} = $ldap;

    my $permission = 0;

    # User is main-user/responsible/landbmanager
    if ( grep { $self->{_USER} eq $_ } @ldapusers ) {

        # FIXME: Log user and method.
        $permission++;
    }

    # User is member of egroup
    unless ( $permission > 0 ) {
        if ( $self->memberof($responsible) ) {
            $permission++;
        }
    }

    # User is member of egroup
    unless ( $permission > 0 ) {
        if ( $self->memberof($mainuser) ) {
            $permission++;
        }
    }

    # User is member of egroup
    unless ( $permission > 0 ) {
        if ( $self->memberof($landbmanager) ) {
            $permission++;
        }
    }

    # aims support
    unless ( $permission > 0 ) {
        if ( $self->memberof( $self->{_CONF}->{'EGROUP_AIMSSUPPORT'} ) ) {
            $permission++;
        }
    }

    # sys admin, machine in 513 or 613 or 773 (Network Hub) or 775 (PDC) or 6045 (LHCb containers)?
    unless ( $permission > 0 ) {
        my $building = $device->{'Location'}->{Building};
        $building =~ s/^0+//;
        $building =~ s/[A-Z]+//;
        if ( grep { $building eq $_ } qw(513 613 773 775 6045) ) {
            if ( $self->memberof( $self->{_CONF}->{'EGROUP_SYSADMINS'} ) ) {
                $permission++;
            }
        }
    }

    # Explicit deny.
    unless ( $permission > 0 ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/permission] Error: Permission denied for $device->{'DeviceName'}."
        );
        die "Error: Permission denied for $device->{'DeviceName'}.\n";
    }

    $self;

}

##############################################
sub GetOwners
##############################################
{
    my ( $self, $device ) = @_;
    unless ( ref($device) and ref($device) eq "HASH" ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/GetOwners] Error: Device struct expected."
        );
        die "Error: Device struct expected.\n";
    }
    my (@owners);
    push( @owners, $device->{'ResponsiblePerson'}->{Email} );
    my $mainuser = $device->{'UserPerson'};
    if ($mainuser) {
        for my $res ($mainuser) {
            push( @owners, $res->{Email} );
        }
    }
    my $landbmanager = $device->{'LandbManagerPerson'};
    if ($landbmanager) {
        for my $res ($landbmanager) {
            push( @owners, $res->{Email} );
        }
    }
    @owners;
}

##############################################
sub memberof
##############################################
{
    my ( $self, $group ) = @_;

    my $member = 0;

    my $ldap = $self->{_LDAP};

    my $rest = '';

    my $ldapbase =
        $self->{_CONF}->{'LDAP_BASE'}
      ? $self->{_CONF}->{'LDAP_BASE'}
      : "DC=cern,DC=ch";
    if ( length($group) != 0 ) {

        ( $group, $rest ) = split( '@', $group );

        # To get a recursive search, or to have AD check relations, extra properties need to be included to the filter.
        # In this case, the string 1.2.840.113556.1.4.1941 will need to be added. According to Microsoft:
        # The string 1.2.840.113556.1.4.1941 specifies LDAP_MATCHING_RULE_IN_CHAIN. This applies only to DN attributes.
        # This is an extended match operator that walks the chain of ancestry in objects all the way to the root until it finds a match.
        # This reveals group nesting. It is available only on domain controllers with Windows Server 2003 SP2 or Windows Server 2008 (or above).
        my $result = $ldap->ldap_search(
            $ldapbase,
"(&(cn=$self->{_USER})(memberOf:1.2.840.113556.1.4.1941:=CN=$group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch))",
            ('cn')
        );
        my @result = $ldap->fetch_values( $result, ('cn') );

        for my $user (@result) {
            $user =~ s/^CN=//;
            if ( $self->{_USER} eq $user ) {
                $member = 1;
                last;
            }
        }
    }

    $member;
}

##############################################
sub validate_ks
##############################################
{
    my ( $self, $kickstart ) = @_;

    # Check somethings about the kickstart.
    # Does it exist?
    # $kickstart = location (FIXME: or file?)

    if ( $kickstart =~ m/^http:/ ) {

        # Fetch the kickstart file by http.
        $kickstart = $self->get_http_kickstart($kickstart);
    }
    elsif ( $kickstart =~ m/^\/afs\// ) {

        # Fetch the kikcstart file from afs
        $kickstart = $self->get_afs_kickstart($kickstart);
    }

    # Kickstart is now a FILE

    # Return the contents of the kickstart (validated).
    $kickstart = encode_base64($kickstart);

    $kickstart;
}

##############################################
sub get_http_kickstart
##############################################
{
    my ( $self, $ks ) = @_;

    unless ($ks) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_http_kickstart] Error: Kickstart cannot be null."
        );
        die "Error: Kickstart cannot be null.\n";
    }

    # FIXME: What about https?
    my $request  = HTTP::Request->new( GET => $ks );
    my $ua       = LWP::UserAgent->new;
    my $response = $ua->request($request);

    # FIXME: If not a success, be more descriptive (404?, permission denied? etc)
    unless ( $response->is_success ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_http_kickstart] Error: Cannot download kickstart from $ks."
        );
        die "Error: Cannot download kickstart from $ks.\n";
    }

    my $kickstart = $response->content;

    $kickstart;
}

##############################################
sub get_afs_kickstart
##############################################
{
    my ( $self, $ks ) = @_;

    unless ($ks) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [host/get_afs_kickstart] Error: Kickstart location cannot be null."
        );
        die "Error: Kickstart location cannot be null.\n";
    }

    my $kickstart;

    open( *ks, '<' . $ks ) or die "Error: Unable to open $ks: $!.\n";
    read( *ks, $kickstart, -s *ks );
    close(*ks);

    $kickstart;
}

##############################################
sub validate_kopts
##############################################
{
    my ( $self, $kopts ) = @_;

    # Validate (sanitise) kernel options

    #unless( not $kopts){
    #   my(@kopts) = split("\s", $kopts);
    #}

    $kopts;
}
