##############################################
package aims2db;
##############################################

##############################################
#
# db.pm - db connector.
# Authors: Daniel Juarez <djuarezg@cern.ch>
#          Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#          Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use DBI;

1;

##############################################
sub new
##############################################
{
    my ($class) = @_;
    my ($self)  = bless {}, $class;
    return $self;
}

##############################################
sub Connect
##############################################
{
    my ( $self, $db, $user, $pass, %attrs ) = @_;

    # Connect to the database and return a handle.
    unless ( $db && $user && $pass && %attrs ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [db/Connect] Error: Database connection params are undefined."
        );
        die "Error: Database connection params are undefined.\n";
    }

    # FIXME: We are not very forgiving.
    # If the database connection fails, we just give up.
    # But some nice retry logic might be useful.
    my $dbh;
    eval { $dbh = DBI->connect( $db, $user, $pass, \%attrs ); };
    if ($@) {
        if ( $@ =~ DBI->err ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [db/Connect]Error: Failed to connect to database. $@"
            );
            die "Error: Failed to connect to database. $@\n";
        }
    }
    return $dbh;
}

##############################################
sub Disconnect()
##############################################
{
    my ($self) = @_;
    my $dbh = $self->{_DB};
    $dbh->disconnect if $dbh;
    $self;
}
