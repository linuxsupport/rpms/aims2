#!/usr/bin/perl -w

package aims2ldap;

######################################
#
# aims2ldap.pm - aims2 LDAP Class.
# Authors:
#    Dan Dengate <Dan.Dengate@CERN.CH>
#    Daniel Juarez Gonzalez <djuarezg@cern.ch>
#
######################################

1;

use strict;
use Net::LDAP;
use Net::LDAP::Util qw(ldap_error_text);

######################################
sub new
######################################
{
    my ($class) = @_;
    my ($self)  = bless {}, $class;
    $self;
}

######################################
sub ldap_connect
######################################
{
    my ( $self, $server, $ldap_bind_user, $ldap_bind_pass ) = @_;
    unless ( $server && $ldap_bind_user && $ldap_bind_pass ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ldap/ldap_connect] Error: LDAP Connection String is incorrect."
        );
        die "Error: LDAP Connection String is incorrect.\n";
    }

    # Start to build the ldap object.
    # Connect to LDAP.
    my $ldap = Net::LDAP->new($server)
      or die "Error: Cannot connect to LDAP servers.\n";

    # Bind using credentials.
    my $bind = $ldap->bind(
        "CN=" . $ldap_bind_user . ",OU=Users,OU=Organic Units,DC=cern,DC=ch",
        password => $ldap_bind_pass );
    if ( $bind->code ) {
        my $error = $bind->code;

        #print "Error: Failed to authenticate to LDAP.\n";
        my $error_string = ldap_error_text($error);
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ldap/ldap_connect] Error: Failed to authenticate to LDAP: $error_string."
        );
        die "Error: Failed to authenticate to LDAP: $error_string";
    }

    # Object ready :)
    $self->{LDAP} = $ldap;

    $self;
}

######################################
sub ldap_search
######################################
{
    my ( $self, $base, $filter, $attrs ) = @_;

    unless ($base) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ldap/ldap_search] Error: LDAP basedn undefined."
        );
        die "Error: LDAP basedn undefined.\n";
    }
    unless ($filter) {
        aims2helpers::alog(
            "$ENV{REMOTE_ADDR} - [ldap/ldap_search] Error: No filter provided."
        );
        die "Error: No filter provided.\n";
    }

    my $ldap = $self->{LDAP};

    # search the tree.
    my $result =
      $ldap->search( base => "$base", scope => "sub", filter => "$filter" );

    # 'pointer' to entry
    return $result;
}

######################################
sub ldap_disconnect
######################################
{
    my ($ldap) = @_;
    $ldap->unbind;
}

######################################
sub fetch_values
######################################
{
    my ( $self, $result, @attrs ) = @_;
    unless ($result) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ldap/ldap_search] Error: Result struct needed to get values from."
        );
        die "Error: Result struct needed to get values from.\n";
    }
    if ( !@attrs ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ldap/ldap_search] Error: No attributes specified for fetching."
        );
        die "Error: No attributes specified for fetching.\n";
    }
    my @entries = $result->entries;
    my @values;
    foreach my $entry (@entries) {
        push( @values, map { $entry->get_value($_) } @attrs );
    }
    @values;
}

