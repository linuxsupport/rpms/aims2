##############################################
package aims2landb;
##############################################

##############################################
#
# landb.pm - LANDB requester
#
# Authors:
#         Daniel Juarez Gonzalez <djuarezg@cern.ch>
#         Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#         Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use Socket qw(getaddrinfo getnameinfo);
use SOAP::Lite;

1;

##############################################
sub new
##############################################
{
    my ($class) = @_;
    my ($self)  = bless {}, $class;
    return $self;
}

##############################################
sub build_token
##############################################
{
    my ( $self, $endpoint, $proxy, $schema, $user, $pass ) = @_;

    unless ( $endpoint && $proxy && $schema && $user && $pass ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/build_token] Error: build_token failed. Some configuration values undefined."
        );
        die "Error: build_token failed. Some configuration values undefined.\n";
    }

    my $conn = SOAP::Lite->uri($endpoint)->xmlschema($schema)
      ->proxy( $proxy, keep_alive => 1 );

    unless ($conn) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/build_token] Error: Connection to Network Database failed."
        );
        die "Error: Connection to Network Database failed.\n";
    }

    my $call = $conn->getAuthToken( $user, $pass, 'NICE' );
    if ( $call->fault ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/build_token] Error: Fault getting token for Network Database."
        );
        die "Error: Fault getting token for Network Database.\n";
    }
    my ($auth) = $call->result;

    my $AuthToken = SOAP::Header->name( 'Auth' => { "token" => $auth } );
    $self->{_TOKEN} = $AuthToken;

    $self->{_LANDB} = $conn;

}

##############################################
sub get_device_by_name
##############################################
{

    my ( $self, $host ) = @_;

    # Fetch a device struct from database.

    unless ($host) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/get_device_by_name] Error: Cannot query for device without input."
        );
        die "Error: Cannot query for device without input.\n";
    }

    $host =~ tr/[a-z]/[A-Z]/;

    my $conn  = $self->{_LANDB};
    my $token = $self->{_TOKEN};

    my $res1 = $conn->searchDevice( $token, { Name => $host } );
    if ( $res1->fault ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/get_device_by_name] Error: Device $host not found in the Network Database."
        );
        die "Error: Device $host not found in the Network Database.\n";
    }
    my $dev1 = $res1->result->[0];

    my $result = $conn->getDeviceInfo( $token, $dev1 );
    if ( $result->fault ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [landb/get_device_by_name] Error: Cannot get device information for $host."
        );
        die "Error: Cannot get device information for $host.\n";
    }

    my $device = $result->result;

    $device;

}

##############################################
sub get_device_by_ip
##############################################
{

    my ( $self, $ip ) = @_;

    # Fetch a device struct from database.

    unless ($ip) {
        die "Error: Cannot query for device without input.\n";
    }

    my $conn  = $self->{_LANDB};
    my $token = $self->{_TOKEN};

    my $res1 = $conn->searchDevice( $token, { IPAddress => $ip } );
    if ( $res1->fault ) {
        die
          "Error: Device with IP \"$ip\" not found in the Network Database.\n";
    }
    my $dev1 = $res1->result->[0];

    my $device;
    # Devices without a fixed IP are referred to in LanDB as "Portable Devices" (PB-D)
    # We cannot query these via the landb API - however, we can still get the device
    # name via DNS lookups
    if ($dev1 =~ /^PB-D/) {
        my $err;
        my @addrs;
        ( $err, @addrs ) = getaddrinfo($ip);
        if ($err) {
            die "Error: Cannot query ${ip} in DNS\n";
         }
        my $hostname;
        ( $err, $hostname ) = getnameinfo( $addrs[0]->{addr} );
        if ($err) {
            die "Error: Cannot find a hostname in DNS for ${ip}\n";
        }
	# ks.cgi expects just the shortname
	# strip out .dyndns.cern.ch / .cern.ch
	$hostname =~ s/\..*//;
	$device = { 'DeviceName' => $hostname };
    # Not a portable device, extract the devicename via landb and not dns
    } else {
        my $result = $conn->getDeviceInfo( $token, $dev1 );
        if ( $result->fault ) {
            die "Error: Cannot get device information for IP \"$ip\".\n";
        }
        $device = $result->result;
    }

    $device;

}
