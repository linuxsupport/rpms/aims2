##############################################
package aims2soap;
##############################################

use strict;
use Data::Dumper;
use SOAP::Lite;
use aims2server::server;
use vars qw(@ISA);
@ISA = qw(aims2server);

1;

##############################################
BEGIN {
##############################################
    no strict 'refs';

    # These are our callable methods. Anything else will croak below.
    # WATCH OUT, THESE METHODS NEED TO BE DEFINED IN SERVER.PM AS WELL
    for my $Method (
        qw(AddHost RemoveHost GetHostByName AddImage RemoveImage GetImageByName ListAllImages EnablePXE DisablePXE HostHistory GetKickstartFile UpdateKickstartFile Permission)
      )
    {
        # Try to execute the method...
        eval "sub $Method";
        *$Method = sub {

            # $self is a bless object from aims2server->new()
            my $self = shift->new(@_);
            die SOAP::Fault->faultcode('Server.RequestError')
              ->faultstring("Request Error: Could not find object.")
              unless $self;

            # FIXME (bookmark)
            #die Dumper($self);

            # $res is the result from the method call.
            my $SuperMethod = "SUPER::$Method";
            my $res         = $self->$SuperMethod(@_);
            die SOAP::Fault->faultcode('Server.ExecError')
              ->faultstring("Execution error: $res.")
              unless $res;

            $res;

        };
    }
}

##############################################
sub new
##############################################
{

    # Think of this as aims2server->new(@_);
    my $class = shift;
    return $class if ref($class);
    my $self = $class->SUPER::new(@_);
    $self;
}
