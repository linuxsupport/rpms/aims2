##############################################
package aims2server;
##############################################

##############################################
#
# Main AIMS2 logic
#
# Authors: Daniel Juarez <djuarezg@cern.ch>
#         Jaroslaw Polok <Jaroslaw.Polok@cern.ch>
#         Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use DBI;
use DBD::Pg;
use MIME::Base64;
use Authen::Krb5;
use File::Basename;
use File::Spec;
use POSIX qw(setsid strftime);
use Sys::Syslog qw(:standard :macros);
use Digest::MD5 qw(md5_hex);
use File::Path;
use IO::File;
use Digest::MD5;
use Date::Manip;
use Date::Parse;
use File::Find::Rule;

# Temp logging for https://its.cern.ch/jira/browse/LOS-688
use DBI::Log timing => 1;

##############################################
# WATCH OUT, THESE METHODS NEED TO BE DEFINED IN SOAP.PM AS WELL
use subs
  qw(new SetUser AddImage RemoveImage GetImageByName AddHost RemoveHost GetHostByName EnablePXE DisablePXE GetKickstartFile UpdateKickstartFile RemoveKickstartFile Permission);
##############################################

# Server Classes.
use aims2server::db;
use aims2server::conf;
use aims2server::landb;
use aims2server::host;
use aims2server::image;
use aims2server::pxe;

use MIME::Entity;
use vars qw(@ISA);
@ISA = qw(SOAP::Server::Parameters);

my $safe_filename_characters = "a-zA-Z0-9_.-";
my $TFTP_ROOT                = "/aims_share/tftpboot/aims";
my $TMP_UPLOAD               = "/aims_share/tftpboot/aimshttpupload";
my $TFTP_BOOT                = "/boot";
my $SERVER =
  $aims2conf::aims2_server ? $aims2conf::aims2_server : "aims.cern.ch";
my $KSURL = "http://" . $SERVER . "/aims/ks";

1;

##############################################
sub new
##############################################
{
    my ( $class, %args ) = @_;
    my ($self) =
      bless { _CONF => undef, _DB => undef, _SERVER => undef, _USER => undef },
      $class;
    $self->SetServer;
    $self->ConnectToDatabase;
    $self->SetConf;
    $self->SetUser( $args{'username'}, $args{'password'} );
    $self;
}

##############################################
sub SetConf
##############################################
{

    # Read AIMS2 config files and DB conf values

    my ($self) = @_;

    my $dbh = $self->{_DB};
    unless ($dbh) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetConf] Error: Database connection has dropped."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Database connection has dropped.\n";
    }

    my $conf = aims2conf->new($dbh);
    $self->{_CONF} = $conf->get;

    $self;
}

##############################################
sub ConnectToDatabase
##############################################
{
    my ($self) = @_;

    my $dbconn =
        $aims2conf::db_conn_string
      ? $aims2conf::db_conn_string
      : die "Error: Database server undefined.\n";
    my $dbuser =
        $aims2conf::db_user
      ? $aims2conf::db_user
      : die "Error: Database user undefined.\n";
    my $dbpass =
        $aims2conf::db_pass
      ? $aims2conf::db_pass
      : die "Error: Database password undefined.\n";
    my %dbattrs =
        %aims2conf::db_conn_attrib
      ? %aims2conf::db_conn_attrib
      : die "Error: Database attributes undefined.\n";

    my $db  = aims2db->new();
    my $dbh = $db->Connect( $dbconn, $dbuser, $dbpass, %dbattrs );

    $self->{_DB} = $dbh;

    $self;
}

##############################################
sub SetServer
##############################################
{
# Set the specific user to use. Useful for testing or pointing to specific servers for debugging

    my ($self) = @_;

    my $server = `hostname -f`;
    chomp($server);
    $self->{_SERVER} = $server;

    my $client = $ENV{'REMOTE_HOST'};
    $self->{_CLIENT} = $client;

    $self;
}

##############################################
sub SetUser
##############################################
{
    # Authenticate and set the user array.

    my ( $self, $username, $password ) = @_;

    # Credentials must be presented.
    unless ( $username and $password ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] No authentication credentials presented."
        );
        die "No authentication credentials presented.\n";
    }

    # The password given to us by the client is base64 encoded... we hope.
    $password = decode_base64($password);

    # Kerberos configuration values.
    my $version =
        $self->{_CONF}->{'KRB_VERSION'}
      ? $self->{_CONF}->{'KRB_VERSION'}
      : "aims2_4";
    my $service =
        $self->{_CONF}->{'KRB_SERVICE'}
      ? $self->{_CONF}->{'KRB_SERVICE'}
      : "aims";
    my $kt_file =
        $self->{_CONF}->{'KRB_KEYTAB'}
      ? $self->{_CONF}->{'KRB_KEYTAB'}
      : "/etc/krb5.keytab.aims";

    # Make sure that the $version strings match, and seperate from password.
    if ( $password =~ /^$version=(.*)/s ) {
        ( $version, $password ) = split( $version . "=", $password );
    }
    else {
        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] ERROR: You are using an out-of-date AIMS client. Please update."
        );
        die "ERROR: You are using an out-of-date AIMS client. Please update.\n";
    }

    # Initiate and create a new Auth Context Object.
    Authen::Krb5::init_context();
    my $ac = new Authen::Krb5::AuthContext;
    unless ($ac) {

        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] Error: Cannot create a new Authentication Context."
        );
        die "Error: Cannot create a new Authentication Context.\n";
    }

    # Resolve a service principal.
    my $sp = Authen::Krb5::sname_to_principal( $self->{_SERVER}, $service,
        KRB5_NT_SRV_HST );
    unless ($sp) {

        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] Error: Cannot resolve to a service principal."
        );
        die "Error: Cannot resolve to a service principal.\n";
    }

    # Resolve to the keytab file.
    my $kt = Authen::Krb5::kt_resolve("FILE:$kt_file");
    unless ($kt) {

        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] Error: Keytab file not available."
        );
        die "Error: Keytab file not available.\n";
    }

    # Try to decrypt the ticket.
    my $t = Authen::Krb5::rd_req( $ac, $password, $sp, $kt );
    unless ($t) {

        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
            "$ENV{REMOTE_ADDR} - [server/SetUser] Authentication Failure: ",
            Authen::Krb5::error(), "." );
        die "Authentication Failure: ", Authen::Krb5::error(), ".\n";
    }

    my $user = $t->enc_part2->client->data;
    unless ($user) {

        # This will return exit code 1. Watch out as integrations parse this output
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/SetUser] Error: Couldn't extract user from ticket."
        );
        die "Error: Couldn't extract user from ticket.\n";
    }

    undef($ac);
    Authen::Krb5::free_context();

    $self->{_USER} = $user;

    $self;

}

##############################################
sub AddImage
##############################################
{
    # Upload a new image to aims2.

    # This method depends on upload.cgi to handle HTTP uploads
    # We basically send files, randomize its names, return paths so we can indicate the server where to look
    # for the blobs before serving them from TFTP/HTTP, same logic as before, but avoiding XML parsing that makes
    # memory consumption explode

    my ( $self, %args ) = @_;
    my $dbh = $self->{_DB};

    # Blobs to store
    my $vmlinuz_blob = undef;
    my $initrd_blob  = undef;
    my $vmlinuz_sum  = undef;
    my $vmlinuz_size = undef;
    my $initrd_sum   = undef;
    my $initrd_size  = undef;

    unless (%args) {
        die "Error: Input cannot be null.\n";
    }

    # First of all do not waste time if user does not have permissions for uploading images
    my $pxeboot =
      aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    $pxeboot->upload_permission;

    # Name of the image.
    my $name =
        $args{'name'}
      ? $args{'name'}
      : die "Error: No image name was provided.\n";
    $name = $self->validate($name);
    $name =~ tr/[a-z]/[A-Z]/;

    # Short description of the image.
    my $desc =
        $args{'desc'}
      ? $args{'desc'}
      : die "Error: No image description was provided.\n";
    $desc = $self->validate($desc);
    $desc =~ tr/[a-z]/[A-Z]/;

    # Arch of the image.
    my $arch = $args{'arch'};
    unless ( $arch
        and grep { $arch eq $_ } qw(i386 i686 x86_64 aarch64 armv7 armv8) )
    {
        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Bad values provided for arch.\n";
    }

    # Extra bits for an image.
    my $kopts  = $args{'kopts'}   ? $args{'kopts'}   : undef;
    my $groups = $args{'egroups'} ? $args{'egroups'} : undef;
    my $uefi   = $args{'uefi'}    ? $args{'uefi'}    : undef;

    # Validate user-given filenames so we do not get nasty things
    $args{'vmlinuz_filename'} = validate_filename( $args{'vmlinuz_filename'} );
    $args{'initrd_filename'}  = validate_filename( $args{'initrd_filename'} );

    # We need these paths so we can read blobs and store them in the TFTP/HTTP dir structure
    # These args were returned from upload.cgi as an intermediate response after successfull upload
    # Do not use user-given filenames as they are, join them with the restricted tmp path to avoid any tampering
    my $vmlinuz_http_path =
        $args{'vmlinuz_filename'}
      ? $TMP_UPLOAD . "/$args{'vmlinuz_filename'}"
      : undef;
    my $initrd_http_path =
        $args{'initrd_filename'}
      ? $TMP_UPLOAD . "/$args{'initrd_filename'}"
      : undef;

    if ( !-e $vmlinuz_http_path ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddImage] Error: $vmlinuz_http_path file not readable."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $vmlinuz_http_path file not readable.\n";
    }

    if ( defined($initrd_http_path) && !-e $initrd_http_path ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddImage] Error: $initrd_http_path file not readable."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $initrd_http_path file not readable.\n";
    }
    open( *VMLINUZ, '<' . $vmlinuz_http_path )
      or die "Error: Unable to open file - " . $vmlinuz_http_path . ": $!.\n";
    {
        use bytes;
        read( *VMLINUZ, $vmlinuz_blob, -s $vmlinuz_http_path );
    }
    close(*VMLINUZ);
    if ( !$vmlinuz_blob ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddImage] Error: Unable to read file - "
              . $vmlinuz_http_path
              . "." );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Unable to read file - " . $vmlinuz_http_path . ".\n";
        exit 1;
    }

    $vmlinuz_sum = md5_hex( $vmlinuz_blob );
    $vmlinuz_size = length( $vmlinuz_blob );

    if ( defined($initrd_http_path) ) {
        open( *INITRD, '<' . $initrd_http_path )
          or die "Error: Unable to open file - "
          . $initrd_http_path
          . ": $!.\n";
        {
            use bytes;
            read( *INITRD, $initrd_blob, -s $initrd_http_path );
        }
        close(*INITRD);
        if ( !$initrd_blob ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddImage] Error: Unable to read file - "
                  . $initrd_http_path
                  . "." );

            # This will return exit code 1. Watch out as integrations parse this output
            die "Error: Unable to read file - " . $initrd_http_path . ".\n";
            exit 1;
        }
        $initrd_sum = md5_hex( $initrd_blob );
        $initrd_size = length( $initrd_blob );
    }

    # This will return exit code 1. Watch out as integrations parse this output
    die "Error: No kernel provided.\n"      if ( !defined($vmlinuz_http_path) );
    die "Error: No kernel file provided.\n" if ( !defined($vmlinuz_blob) );

    # We no longer store image blobs in the database, we directly put them in the BOOT dir
    # Ceph team already does backups of our share, including all of our images
    $pxeboot->add(
        {
            NAME         => $name,
            ARCH         => $arch,
            DESC         => $desc,
            KOPTS        => $kopts,
            GROUPS       => $groups,
            VMLINUZ      => $args{'vmlinuz'},
            VMLINUZ_SUM  => $vmlinuz_sum,
            VMLINUZ_SIZE => $vmlinuz_size,
            INITRD       => $args{'initrd'},
            INITRD_SUM   => $initrd_sum,
            INITRD_SIZE  => $initrd_size,
            UEFI         => $uefi,
        }
    );

    # Dump image on disk, once done it is ready to be used
    build_pxeboot( $dbh, $name, $vmlinuz_blob, $vmlinuz_sum, $initrd_blob, $initrd_sum );
    aims2helpers::alog(
        "$ENV{REMOTE_ADDR} - [server/AddImage] Image $name uploaded to aims.");

    # This will return exit code 0. Watch out as integrations parse this output
    "Image $name uploaded to aims.";

}

##############################################
sub RemoveImage
##############################################
{
    # Remove (erase) pxeboot media from aims2.

    my ( $self, %args ) = @_;
    my $dbh = $self->{_DB};

    my $name =
        $args{'imagename'}
      ? $args{'imagename'}
      : die "Error: Cannot remove pxe media. NAME is undefined.\n";
    $name = $self->validate($name);
    $name =~ tr/[a-z]/[A-Z]/;

    my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );

    # Only the owner and linuxsoft can remove the image.
    my $pxeboot = $image->get_pxeboot_by_name($name);
    my $owner   = $pxeboot->{$name}{OWNER};
    my $groups  = $pxeboot->{$name}{GROUPS};
    unless ($owner) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/RemoveImage] Error: $name does not exist."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $name does not exist.\n";
    }
    $image->permission( $owner, $groups );
    $image->remove($name);

    # Remove image from disk, it should no longer be in the system
    remove_pxeboot( $dbh, $name );
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/RemoveImage] Image $name removed from aims."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Image $name removed from aims.";
}

##############################################
sub GetImageByName
##############################################
{
    # Fetch pxeboot entries matching $name.

    my ( $self, %args ) = @_;
    my $name =
        $args{'imagename'}
      ? $args{'imagename'}
      : die "Error: Cannot search for pxeboot. NAME is undefined.\n";

    $name = $self->validate($name);
    $name =~ tr/[a-z]/[A-Z]/;

    my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    my $pxeboot = $image->get_pxeboot_by_name($name);

    $pxeboot;
}

##############################################
sub AddHost
##############################################
{
    # Add (register) a device on aims2.

    my ( $self, %args ) = @_;
    my $dbh = $self->{_DB};

    # Clean up input data.
    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddHost] Error: Cannot remove host. Hostname is undefined."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Cannot remove host. Hostname is undefined.\n";
    }

    # Only use shortnames, not fqdn, for consistency. Also this avoids human errors
    $hostname = validate_hostname($hostname);

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddHost] Starting $hostname host addition."
    );

    my $kickstart =
      $self->validate( $args{'kickstart'} ) ? $args{'kickstart'} : undef;
    my $kopts = $self->validate( $args{'kopts'} ) ? $args{'kopts'} : undef;

    # Do not take name from LanDB yet.. we may be dealing with an Interface alias.
    my $device = $self->GetDevice($hostname);

    # AIMS2 always saves strings in uppercase
    $hostname =~ tr/[a-z]/[A-Z]/;

    # Register the device in the aims2 database.
    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    $host->permission($device);
    $host->remove($hostname);
    $host->add( $hostname, $device, $kickstart, $kopts );

    # Kickstart CLOB?
    if ( $args{'ksclob'} ) {
        $kickstart = $args{'ksclob'};
        $host->insert_ks_record( $hostname, $kickstart, "Uploaded" );
    }

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/AddHost] Host $hostname registered with aims."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Host $hostname registered with aims.";
}

##############################################
sub RemoveHost
##############################################
{
    # Remove (de-register) a device from aims2.
    my ( $self, %args ) = @_;
    my $dbh = $self->{_DB};

    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/RemoveHost] Error: Add host failed. No hostname specified."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Add host failed. No hostname specified.\n";
    }

    # Only use shortnames, not fqdn, for consistency. Also this avoids human errors
    $hostname = validate_hostname($hostname);

    # AIMS2 always saves strings in uppercase
    $hostname =~ tr/[a-z]/[A-Z]/;

    # If we run this check, Ironic will face issues with nodes no longer existing
    # on LANDB, and therefore will be unable to delete them.
    #my $device = $self->GetDevice($hostname);

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    my $reg  = $host->get_device_by_name($hostname);
    unless ( values %$reg ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/RemoveHost] Error: $hostname is not registered with AIMS."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $hostname is not registered with AIMS.\n";
    }

    # If we run this check, Ironic will face issues with nodes no longer existing
    # on LANDB, and therefore will be unable to delete them.
    #$host->permission($device);

    my $pxe = aims2pxe->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );

    # This will also remove all config files that correspond to the host
    $pxe->disable($hostname);

    # We only remove the entry once we got rid of configs!
    $host->remove($hostname);

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/RemoveHost] Host $hostname removed from AIMS."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Host $hostname removed from AIMS.";
}

##############################################
sub GetHostByName
##############################################
{
    # Return a struct (reference) containing host data.

    my ( $self, %args ) = @_;

    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/GetHostByName] Error: Cannot fetch host. No hostname provided."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Cannot fetch host. No hostname provided.\n";
    }
    $hostname = $self->validate($hostname);
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_DB} );
    my ($hosts) = $host->get_device_by_name($hostname);

    $hosts;

}

##############################################
sub GetKickstartFile
##############################################
{
    # Return kickstart to user (if permission).

    my ( $self, %args ) = @_;

    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/GetKickstartFile] Error: Unable to get kickstart. No hostname provided."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Unable to get kickstart. No hostname provided.\n";
    }
    $hostname = $self->validate($hostname);
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $device = $self->GetDevice($hostname);

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    my $reg  = $host->get_device_by_name($hostname);
    unless ( values %$reg ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/GetKickstartFile] Error: $hostname is not registered with AIMS."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $hostname is not registered with AIMS.\n";
    }
    $host->permission($device);

    my $kickstart = $host->get_host_kickstart($hostname);

    $kickstart;
}

##############################################
sub RemoveKickstartFile
##############################################
{
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/GetKickstartFile] Sorry, this feature is currently disabled. Use `updateks` instead."
    );

    # This will return exit code 1. Watch out as integrations parse this output
    die "Sorry, this feature is currently disabled. Use `updateks` instead.\n";
}

##############################################
sub UpdateKickstartFile
##############################################
{
    # Update the kickstart file.

    my ( $self, %args ) = @_;

    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/UpdateKickstartFile] Error: Unable to get kickstart. No hostname provided."
        );
        die "Error: Unable to get kickstart. No hostname provided.\n";
    }
    $hostname = $self->validate($hostname);
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $kickstart =
      $self->validate( $args{'kickstart'} ) ? $args{'kickstart'} : undef;
    my $ksclob = $args{'ksclob'} ? $args{'ksclob'} : undef;

    my $device = $self->GetDevice($hostname);

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    my $reg  = $host->get_device_by_name($hostname);
    unless ( values %$reg ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/UpdateKickstartFile] Error: $hostname is not registered with AIMS."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: $hostname is not registered with AIMS.\n";
    }

    $host->permission($device);

    if ($ksclob) {
        $host->update_ks_record( $hostname, $ksclob, "Uploaded" );
    }
    else {
        $host->updateks( $hostname, $kickstart );
    }

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/UpdateKickstartFile] Kickstart updated for $hostname."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Kickstart updated for $hostname.";
}

##############################################
sub EnablePXE
##############################################
{
    # Enable the device with pxeboot target.
    # It will create all the pxe configs for the device ifaces

    my ( $self, %args ) = @_;

    my $warn     = "";
    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Error: Unable to enable. No hostname provided."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Unable to enable. No hostname provided.\n";
    }
    $hostname = $self->validate($hostname);
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $name = $args{'imagename'};
    unless ($name) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Error: Unable to enable. No pxeboot target provided."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Unable to enable. No pxeboot target provided.\n";
    }

    my $forever  = 0;
    my $noexpiry = $args{'noexpiry'};
    if ( defined($noexpiry) && $noexpiry =~ /^noexpiry$/ ) {
        $forever = 1;
    }

    my $boottype;

    my $type = $args{'type'};
    if ( defined($type) ) {
        if ( $type =~ /^bios$/ || $type =~ /^pxe$/ ) {
            $boottype = 0;
        }
        elsif ( $type =~ /^uefi$/ ) {
            $boottype = 1;
        }
        elsif ( $type =~ /^arm64$/ ) {
            $boottype = 2;
        }
        else {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Error: allowed boot types are: pxe|bios|uefi|arm64, but: $type specified."
            );

            # This will return exit code 1. Watch out as integrations parse this output
            die
"Error: allowed boot types are: pxe|bios|uefi|arm64, but: $type specified.\n";
        }
    }
    else {
        $type     = "bios";
        $boottype = 0;
    }

    $name = $self->validate($name);
    $name =~ tr/[a-z]/[A-Z]/;

    my $image = aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    my $pxeboot = $image->get_pxeboot_by_name($name);
    unless ( $pxeboot->{$name} ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Error: pxeboot target $name does not exist."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: pxeboot target $name does not exist.\n";
    }

    my $device = $self->GetDevice($hostname);

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    $host->permission($device);
    my $reg = $host->get_device_by_name($hostname);
    unless ( values %$reg ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Error: host $hostname is not registered with aims."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: host $hostname is not registered with aims.\n";
    }

    my $pxe = aims2pxe->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );

    if (
        $boottype == 1
        && ( $pxeboot->{$name}{UEFI} != 1
            || !defined( $pxeboot->{$name}{UEFI} ) )
      )
    {
        $warn = "Warning: Non UEFI target selected for UEFI boot";
    }

    # This will also add pxe confs to disk
    $pxe->enable( $hostname, $name, $forever, $boottype );

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/EnablePXE] Host $hostname enabled for $name ($type). $warn."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Host $hostname enabled for $name ($type). $warn";
}

##############################################
sub DisablePXE
##############################################
{
    # Set device to localboot (ie, disable pxe boot)

    my ( $self, %args ) = @_;

    my $hostname = $args{'hostname'};
    unless ($hostname) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/DisablePXE] Error: Unable to get kickstart. No hostname provided."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: Unable to get kickstart. No hostname provided.\n";
    }
    $hostname = $self->validate($hostname);
    $hostname =~ tr/[a-z]/[A-Z]/;

    my $device = $self->GetDevice($hostname);

    my $host = aims2host->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    $host->permission($device);
    my $reg = $host->get_device_by_name($hostname);
    unless ( values %$reg ) {
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/DisablePXE] Error: host $hostname is not registered with AIMS."
        );

        # This will return exit code 1. Watch out as integrations parse this output
        die "Error: host $hostname is not registered with AIMS.\n";
    }

    my $pxe = aims2pxe->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );

    # This will also remove pxe confs from disk
    $pxe->disable($hostname);

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/DisablePXE] Host $hostname set to localboot."
    );

    # This will return exit code 0. Watch out as integrations parse this output
    "Host $hostname set to localboot.";
}

##############################################
sub GetDevice
##############################################
{
    # Get device struct from the network database.

    my ( $self, $hostname ) = @_;

    my $landbep =
        $self->{_CONF}->{'LANDB_EP'}
      ? $self->{_CONF}->{'LANDB_EP'}
      : "http://network.cern.ch/NetworkService";
    my $landbproxy =
        $self->{_CONF}->{'LANDB_PROXY'}
      ? $self->{_CONF}->{'LANDB_PROXY'}
      : "https://network.cern.ch/sc/soap/soap.fcgi?v=6";
    my $landbschema =
        $self->{_CONF}->{'LANDB_SCHEMA'}
      ? $self->{_CONF}->{'LANDB_SCHEMA'}
      : "http://www.w3.org/2001/XMLSchema";
    my $landbuser = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
    my $landbpass =
        $aims2conf::aims2_pass
      ? $aims2conf::aims2_pass
      : die "password undefined";

    my $landb = aims2landb->new();
    $landb->build_token( $landbep, $landbproxy, $landbschema, $landbuser,
        $landbpass );
    my $device = $landb->get_device_by_name($hostname);

    $device;

}

##############################################
sub Permission
##############################################
{
    # Get device struct from the network database.
    my ($self) = @_;

    my $pxeboot =
      aims2image->new( $self->{_DB}, $self->{_USER}, $self->{_CONF} );
    $pxeboot->upload_permission;
    aims2helpers::alog(
        "$ENV{REMOTE_ADDR} - [server/Permission] Proceeding with the upload.");
    "Proceeding with the upload.";
}

##############################################
sub validate
##############################################
{
    my ( $self, $value ) = @_;

    # allow / for PATHS
    # allow * for wilcards
    # allow numbers
    # allow letters
    # allow _
    # allow -
    # allow :

    # 'clean up' input values
    #if(defined($value) && $value ne ""){
    #   if($value =~ /([\w\-\\\/\*\:\-\=]+)$/){
    #      $value = $1;
    #   } else {
    #     die "Error: Validation is a bit strict.\n";
    #   }
    #}

    return $value;
}

##############################################
sub validate_filename
##############################################
{
    my ($filename) = @_;

    if ( defined $filename ) {
        my ( $name, $path, $extension ) = fileparse( $filename, '..*' );

        $filename = $name . $extension;
        $filename =~ tr/ /_/;
        $filename =~ s/[^$safe_filename_characters]//g;

        if ( $filename =~ /^([$safe_filename_characters]+)$/ ) {
            $filename = $1;
        }
        else {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/validate_filename] $filename contains invalid characters."
            );

            # This will return exit code 1. Watch out as integrations parse this output
            die "$filename contains invalid characters.";
        }
    }
    return $filename;
}

##############################################
sub validate_hostname
##############################################
{
    # To be consistent and avoid human error we make sure we use hostname/shortname
    # This means we will need to transform test.cern.ch to test, otherwise it can pose interface collisions
    my ($hostname) = @_;
    my ($shortname);
    if ( defined $hostname ) {

        # Capture only up to the first dot to get the shortname
        ($shortname) = $hostname =~ /([^.]+)/;
        $shortname =~ tr/ /_/;

        # We need to accept valid hostnames which include also unserscores and hyphens
        if ( $shortname =~ m/\A([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}\Z/ ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/validate_hostname] Invalid character found in $shortname."
            );

            # This will return exit code 1. Watch out as integrations parse this output
            die "Invalid character found in $shortname.";
        }
    }
    return $shortname;
}

##############################################
sub get_file_sum
##############################################
{
    # Get an md5sum from the file.
    my $file   = shift;
    my $handle = new IO::File( $file, "r" );
    my $ident  = Digest::MD5->new;
    while (<$handle>) {
        $ident->add($_);
    }
    my $sum = $ident->hexdigest;
    return $sum;
}

# --------------------------------------------
# Image helpers
# --------------------------------------------

##############################################
sub write_file
##############################################
{
    my ( $file, $data, $sum ) = @_;

    my $built = 0;

    # We may have already written the file ok the first time. Check.
    if ( -e $file ) {
        my $existing = get_file_sum($file);
        if ( $existing eq $sum ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/write_file] skipping $file, already built"
            );
            $built = 1;
        }
    }

    # If we haven't.
    unless ( $built > 0 ) {
        my $build = new IO::File ">$file";
        if ( defined $build ) {
            print $build $data;
            $build->close;
        }
        else {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/write_file] Error: cannot write file $file : $!"
            );
        }

        # Check the file built correctly.
        my $SUM = get_file_sum($file);

        unless ( $SUM eq $sum ) {
            aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/write_file] Error: file mismatch with $file ($SUM <> $sum)"
            );
        }
    }
}

##############################################
sub insert0 {
##############################################
    # 7th July =~ 07th July :)
    my ($date) = shift;
    if ( $date < 10 ) {
        return "0$date";
    }
    return $date;
}

##############################################
sub getdate {
##############################################
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $iddst ) =
      localtime(time);
    my $datestring;
    $year += 1900;
    $mon++;
    $mon        = insert0($mon);
    $mday       = insert0($mday);
    $min        = insert0($min);
    $datestring = "$year-$mon-$mday $hour:$min";
    return ($datestring);
}

##############################################
sub groan
##############################################
{
    my $groan = shift;
    aims2helpers::alog("error: $groan");
    die "There was a problem while processing your action, please contact the administrators."
}

##############################################
sub remove_pxeboot
##############################################
{
    my ( $dbh, $NAME ) = @_;
    opendir( *pxedir, $TFTP_ROOT . $TFTP_BOOT )
      or aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/write_file] Error: cannot read contents of TFTP_BOOT directory: $!"
      );
    closedir(*pxedir);
    aims2helpers::alog(
        "$ENV{REMOTE_ADDR} - [server/write_file] Removing pxeboot $NAME");
    my $file = "$TFTP_ROOT$TFTP_BOOT/$NAME";
    rmtree $file or groan("$ENV{REMOTE_ADDR} - [server/remove_pxeboot] Error: failed to remove $file: $!");
}

##############################################
sub build_pxeboot
##############################################
{
    my ( $dbh, $NAME, $VMLINUZ_BLOB, $VMLINUZ_SUM, $INITRD_BLOB, $INITRD_SUM ) = @_;

    # Build the files.
    my $dir   = $TFTP_ROOT . $TFTP_BOOT . "/$NAME";
    unless ( -e $dir ) {
        mkdir $dir or groan("$ENV{REMOTE_ADDR} - [server/build_pxeboot] Error: Cannot create directory for $NAME");
    }

    # Create the kernel.
    write_file(
        "$dir/vmlinuz",
        $VMLINUZ_BLOB,
        $VMLINUZ_SUM
    );
    aims2helpers::alog("$ENV{REMOTE_ADDR} - [server/build_pxeboot] Built vmlinuz for $NAME");

    # Create initrd.img (if present)
    if ( $INITRD_SUM ) {
        write_file(
            "$dir/initrd",
            $INITRD_BLOB,
            $INITRD_SUM
        );
        aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [server/build_pxeboot] Built initrd.img for $NAME"
        );
    }
}
