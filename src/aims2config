#!/usr/bin/perl -w

################################################################################
#
# aims2config.pl - Tool for updating the DB
# from within an AIMS node
# Ref. https://linuxops.web.cern.ch/aims2/aims2server/#server-side-configuration
#
# Author: Daniel Juarez Gonzalez <djuarezg@cern.ch>
#
################################################################################

use strict;
use Getopt::Long;
use DBI;
use DBD::Pg;
use aims2server::conf;
use aims2server::db;
my ( $key, $value, %code );
my $verbose = 0;

##############################################
my %opts = (
##############################################
    'key=s'   => \$key,
    'value=s' => \$value,
    'verbose' => \$verbose
);
my $options = GetOptions(%opts);

##############################################
sub db_connect
##############################################
{
    my $dbconn =
        $aims2conf::db_conn_string
      ? $aims2conf::db_conn_string
      : die "Error: Database server undefined.\n";
    my $dbuser =
        $aims2conf::db_user
      ? $aims2conf::db_user
      : die "Error: Database user undefined.\n";
    my $dbpass =
        $aims2conf::db_pass
      ? $aims2conf::db_pass
      : die "Error: Database password undefined.\n";
    my %dbattrs =
        %aims2conf::db_conn_attrib
      ? %aims2conf::db_conn_attrib
      : die "Error: Database attributes undefined.\n";

    my $db  = aims2db->new();
    my $dbh = $db->Connect( $dbconn, $dbuser, $dbpass, %dbattrs );

    unless ($dbh) {
        die "Error: Cannot establish database connection. Exiting...\n";
    }
    $dbh;
}

##############################################
$code{'create'} = sub()
##############################################
{
    unless ($key) {
        die "Error: Key required.\n";
    }

    unless ($value) {
        die "Error: Value required.\n";
    }

    my $dbh = db_connect;

    my $conf = aims2conf->new($dbh);
    $conf->create( $key, $value );

    print "Key $key created with value $value.\n";

};

##############################################
$code{'remove'} = sub()
##############################################
{
    unless ($key) {
        die "Error: Key required to remove value.\n";
    }

    my $dbh = db_connect;

    my $conf = aims2conf->new($dbh);
    $conf->remove($key);

    print "$key removed.\n";

};

##############################################
$code{'update'} = sub()
##############################################
{
    unless ($key) {
        die "Error: Key required.\n";
    }

    unless ($value) {
        die "Error: Value required.\n";
    }

    my $dbh = db_connect;

    my $conf = aims2conf->new($dbh);
    $conf->update( $key, $value );

    print "Key $key set to $value\n";

};

##############################################
$code{'show'} = sub()
##############################################
{
    unless ($key) {
        die "Error: Cannot query without key.\n";
    }

    my $dbh = db_connect;

    my $conf       = aims2conf->new($dbh);
    my $conf_value = $conf->get_value($key);
    if ( !$conf_value ) {
        die "Error: Key does not exist.\n";
    }
    print "Value of $key is " . $conf_value . "\n";

};

##############################################
$code{'list'} = sub()
##############################################
{
    my $dbh       = db_connect;
    my $conf      = aims2conf->new($dbh);
    my $list_conf = $conf->get;
    while ( my ( $k, $v ) = each(%$list_conf) ) {
        print "$k => $v\n";
    }

};

##############################################
sub init
##############################################
{

    if ( !@ARGV ) {
        print "Usage: $0 list|show|create|remove|update [--key|--value]\n";
        exit 1;
    }
    my $cmd = $ARGV[0];
    unless ($cmd) {
        exit 0;
    }
    $key = $ARGV[1];
    $key =~ tr/[a-z]/[A-Z]/ if $key;

    $value = $ARGV[2] ? $ARGV[2] : undef;

    if ( $code{$cmd} ) {
        $code{$cmd}->();    # Go!
    }
    else {
        exit 1;
    }
}

init();
