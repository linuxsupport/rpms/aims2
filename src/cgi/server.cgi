#!/usr/bin/perl -w
##############################################
# aims2 CGI SOAP Interface.
#
# Authors: Daniel Juarez <djuarezg@cern.ch>
#          Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 
#          Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use aims2server::soap;
use SOAP::Transport::HTTP;

##############################################
SOAP::Transport::HTTP::CGI
##############################################
  ->dispatch_with( { 'urn:/aims' => 'aims2soap' } )
  ->objects_by_reference('aims2soap')->handle;

##############################################
exit;
##############################################
