#!/usr/bin/perl

##############################################
#
# aims2 Post Reboot CGI.
#
# Prevents hosts from going into endless install loops.
# Authors: Daniel Juarez <djuarezg@cern.ch>
#          Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 
#          Dan Dengate <dengate@cern.ch>
#
##############################################

use strict;
use CGI qw(param);
use aims2server::conf;
use aims2server::db;
use aims2server::landb;
use aims2server::host;
use aims2server::pxe;
use aims2server::helpers;

print "Content-type: text/html\n\n";

my $landbep     = "http://network.cern.ch/NetworkService";
my $landbproxy  = "https://network.cern.ch/sc/soap/soap.fcgi?v=6";
my $landbschema = "http://www.w3.org/2001/XMLSchema";
my $landbuser   = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
my $landbpass =
  $aims2conf::aims2_pass ? $aims2conf::aims2_pass : die "password undefined";

my $landb = aims2landb->new();
$landb->build_token( $landbep, $landbproxy, $landbschema, $landbuser,
    $landbpass );
my $device = $landb->get_device_by_ip( $ENV{REMOTE_ADDR} );

my $HOST = $device->{'DeviceName'};

if ( !defined($device) ) {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] Error: No response from LANDB servers."
    );
    print "No response from LANDB servers";
    exit 1;
}

if ( !$HOST ) {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] Error: No hostname obtained from LANDB."
    );
    print "No hostname obtained from LANDB";
    exit 1;
}

# AIMS2 hostnames are always stored in capital letters
$HOST =~ tr/[a-z]/[A-Z]/;

# FIXME: Set all devices to locaboot when hit.

my $dbconn = $aims2conf::db_conn_string;
my $dbuser = $aims2conf::db_user;
my $dbpass = $aims2conf::db_pass;
my %dbattr = %aims2conf::db_conn_attrib;

my $db  = aims2db->new();
my $dbh = $db->Connect( $dbconn, $dbuser, $dbpass, %dbattr );

my $host = aims2host->new( $dbh, "aims2reboot", {} );
my $reg  = $host->get_device_by_name($HOST);
unless ( values %$reg ) {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] Error: $HOST is not registered with aims2. No action taken."
    );
    print "$HOST is not registered with aims2. No action taken.";
    exit 1;
}

my $pxe = aims2pxe->new( $dbh, "aims2reboot", {} );

# This will remove conf files from disk so it goes back to default config, which will
# boot from disk by default
$pxe->disable($HOST);

    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] $HOST installed by AIMS2 at " . localtime()
  . ". Device set to localboot."
    );
print "$HOST installed by AIMS2 at "
  . localtime()
  . "\n\nDevice set to localboot.";
exit 1;

