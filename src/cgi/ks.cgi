#!/usr/bin/perl -w

##############################################
#
# aims2 Kickstart file renderer.
#
# The kickstart file will only be rendered to the correct device.
# It will use the hostname that corresponds to the client
# IP to retrieve its kickstart during pxe boot
#
##############################################

use strict;
use MIME::Base64;
use aims2server::host;
use aims2server::conf;
use aims2server::db;
use aims2server::landb;
use aims2server::helpers;

# print HTML header.
print "Content-type: text/html\n\n";

my $landbep     = "http://network.cern.ch/NetworkService";
my $landbproxy  = "https://network.cern.ch/sc/soap/soap.fcgi?v=6";
my $landbschema = "http://www.w3.org/2001/XMLSchema";
my $landbuser   = $aims2conf::aims2_user ? $aims2conf::aims2_user : "aims";
my $landbpass =
  $aims2conf::aims2_pass ? $aims2conf::aims2_pass : die "password undefined";

my $landb = aims2landb->new();
$landb->build_token( $landbep, $landbproxy, $landbschema, $landbuser,
    $landbpass );
my $device = $landb->get_device_by_ip( $ENV{REMOTE_ADDR} );

my $HOST = $device->{'DeviceName'};

if ( !defined($device) ) {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] Error: No response from LANDB servers."
    );
    print "No response from LANDB servers";
    exit 1;
}

if ( !$HOST ) {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [reboot.cgi] Error: No hostname obtained from LANDB."
    );
    print "No hostname obtained from LANDB";
    exit 1;
}

# AIMS2 hostnames are always stored in capital letters
$HOST =~ tr/[a-z]/[A-Z]/;


# Maybe being contacted by the wrong (but still right) interface.
if ( $HOST =~ /-IPMI$/ ) {
    $HOST = $`;
}

# Thurs 02 Oct 2008
# Devices in LHCb pit
if ( $HOST =~ /-V$/ ) {
    $HOST = $`;
}

# Connect to the database.
my $dbconn  = $aims2conf::db_conn_string;
my $dbuser  = $aims2conf::db_user;
my $dbpass  = $aims2conf::db_pass;
my %dbattrs = %aims2conf::db_conn_attrib;

my $db  = aims2db->new();
my $dbh = $db->Connect( $dbconn, $dbuser, $dbpass, %dbattrs );

# Fetch kickstart array
my $host      = aims2host->new( $dbh, "aims2ks", {} );
my $ks        = $host->get_host_kickstart($HOST);
my $kickstart = @$ks[1];
if ($kickstart) {
    print decode_base64($kickstart);
}
else {
    aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [ks.cgi] Error: kickstart for $HOST not found."
    );
    print "# aims2 ks cgi error: kickstart for $HOST not found.\n";
}
exit 1;
