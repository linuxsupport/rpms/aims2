#!/usr/bin/perl -w

#
# This is a simple upload CGI to control and 
# store uploads, then return randomized file
# path so aims2client can continue with the 
# addimg workflow
#
# Author: Daniel Juarez <djuarezg@cern.ch>
#
##############################################

use strict;
use CGI qw(-debug);
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use File::Spec;
use warnings;
use diagnostics;
use aims2server::helpers;
# Used for randomising filenames and avoid possible symlink attacks
use String::Random;

# Max size we will be accepting 1GB
$CGI::POST_MAX        = 1024 * 1000000000;
$CGI::DISABLE_UPLOADS = 0;                   # Allow uploads explicitely
my $safe_filename_characters = "a-zA-Z0-9_.-";

# By default HTTP uses a private tmp dir, a chroot jail, which you cannot share among processes nor get its randomized absolute path from within the jail
# https://en.wikipedia.org/wiki/Chroot
# https://serverfault.com/questions/912094/apache2-saves-files-on-tmp-in-a-system-private-hash-instead-of-just-saving
# We need to set PrivateTmp=false otherwise we will never be able to get the abs path outside the chroot jail. This is done through the hostgroup puppet config
# Dir is created by puppet, cleaning regularely its contents and randomizing file uploads to minimize security risks with tmp as usual
# Selinux configuration for httpd to be able to access such dir is added through puppet hostgroup config
my $TMP_UPLOAD = "/aims_share/tftpboot/aimshttpupload";

# Used for randomising the uploaded filenames (remember Lopienski's lessons)
my $string_gen = String::Random->new;

my $query = new CGI;

##############################################
sub DownloadParam
##############################################
{
    my ( $image_name, $param ) = @_;

    my ( $name, $path, $extension ) = fileparse( $image_name, '..*' );
    $image_name = $name . $extension;
    $image_name =~ tr/ /_/;
    $image_name =~ s/[^$safe_filename_characters]//g;

    if ( $image_name =~ /^([$safe_filename_characters]+)$/ ) {
        $image_name = $1;
    }
    else {
        aims2helpers::alog(
        "$ENV{REMOTE_ADDR} - [upload.cgi] Error: Image name contains invalid characters."
        );
        die "Image name contains invalid characters";
    }

    my $upload_param_filehandle = $query->upload($param);

    # Catch interrupted uploads
    if ( !$upload_param_filehandle && $query->cgi_error ) {
        print $query->header( -status => $query->cgi_error );
        exit 0;
    }

    my $param_tmpfilename = $query->tmpFileName($upload_param_filehandle);

    # Size in bytes, retrieved from temporary file handle, before final storing
    my $param_file_size = ( -s $param_tmpfilename );

    if ( $param_file_size > $CGI::POST_MAX ) {
        print $query->header( -status => '400 Bad Request' );
        aims2helpers::alog(
        "$ENV{REMOTE_ADDR} - [upload.cgi] Error: File size exceeds $CGI::POST_MAX bytes, please contact administrators."
        );
        print
"File size exceeds $CGI::POST_MAX bytes, please contact administrators.";
        exit 0;
    }

# As we are using the shared tmp dir we need to randomise filenames or we are at risk (we probably are anyway already, but...)
    my $randomised_filename = $string_gen->randregex('[A-Za-z]{18}');
    my $temporary_param_upload_path =
      "$TMP_UPLOAD/$randomised_filename-$image_name-$param";

    open( PARAM_UPLOADFILE, ">", $temporary_param_upload_path )
      or die("Can't create \"$temporary_param_upload_path\": $!\n");
    binmode PARAM_UPLOADFILE;

    while (<$upload_param_filehandle>) {
        print PARAM_UPLOADFILE;
    }

    close PARAM_UPLOADFILE;

    # Do not return the complete path, just the temporary filename
    "$randomised_filename-$image_name-$param";
}

# Although we do not need the real image name, adding it somewhere on the stored name might help future debugging
my $image_name       = $query->param("imagename");
my $vmlinuz_filename = $query->param("vmlinuz");
my $initrd_filename  = $query->param("initrd");

if ( !$vmlinuz_filename && !$initrd_filename || !$image_name ) {
    print $query->header( -status => '400 Bad Request' );
    aims2helpers::alog(
    "$ENV{REMOTE_ADDR} - [upload.cgi] Error: There was a problem uploading your image."
    );
    print "There was a problem uploading your image.";
    exit 0;
}

my $temporary_upload_name = undef;

if ($vmlinuz_filename) {
    $temporary_upload_name = DownloadParam( $image_name, "vmlinuz" );
}
elsif ($initrd_filename) {
    $temporary_upload_name = DownloadParam( $image_name, "initrd" );
}
else {
    print $query->header( -status => '400 Bad Request' );
    aims2helpers::alog(
    "$ENV{REMOTE_ADDR} - [upload.cgi] Error: There was a problem uploading your image."
    );
    print "There was a problem uploading your image.";
    exit 0;
}

if ($temporary_upload_name) {
    print $query->header( -status => '200 OK' );

# Http response content will be the temporary location of the uploaded file, so next client call tells server where to take
# the image from, store it as a blob in the DB and then get rid of this temporary file
# Note: Files will eventually be cleaned up by cron{'tmp_cleanup' defined in the Puppet manifest
    print "$temporary_upload_name";
    exit 0;
}
else {
    print $query->header( -status => '400 Bad Request' );
    aims2helpers::alog(
    "$ENV{REMOTE_ADDR} - [upload.cgi] Error: There was a problem uploading your image."
    );
    print "There was a problem uploading your image.";
    exit 0;
}

print $query->header( -status => '500 Internal Server Error' );
aims2helpers::alog(
"$ENV{REMOTE_ADDR} - [upload.cgi] Error: There was a problem uploading your image."
);
print "There was a problem uploading your image.";
exit 1;
